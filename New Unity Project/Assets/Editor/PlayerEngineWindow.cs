﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class PlayerEngineWindow : EditorWindow
{
    #region Window Vars
    public List<GameObject> waypointObjects;

    PlayerRailEngine engine;
    string[] waypointNames;
    string[] focusPointNames;
    string[] effectNames;
    
    int waypointIndex = 0;
    int focusPointsIndex = 0;
    int effectsIndex = 0;

    bool debugMode = false;

    static PlayerEngineWindow window;
    #endregion

    public static void OpenRailEngineWindow()
    {
        window = (PlayerEngineWindow)EditorWindow.GetWindow(typeof(PlayerEngineWindow));
        window.titleContent.text = "Engine Editor";
        window.minSize = new Vector2(550, 650);
        window.maxSize = window.minSize;
        window.Show();
    }

    void OnFocus()
    {
        engine = GameObject.Find("Player Rail Engine").GetComponent<PlayerRailEngine>();
        waypointObjects = engine.waypoints;

        if (waypointIndex > waypointObjects.Count - 1)
            waypointIndex = waypointObjects.Count - 1;

        if (waypointObjects == null)
            waypointObjects = new List<GameObject>();
        if (waypointObjects.Count < 1)
        {
            GameObject newWaypoint = new GameObject();
            newWaypoint.AddComponent<WaypointScript>();
            newWaypoint.GetComponent<WaypointScript>().InitializeWaypoint();
            waypointObjects.Add(newWaypoint);
            waypointNames = new string[1];
            waypointNames[0] = "Waypoint 1";
        }
        
        waypointNames = new string[waypointObjects.Count];
        for (int i = 0; i < waypointNames.Length; i++)
            waypointNames[i] = waypointObjects[i].name;
    }

    void OnDestroy()
    {
        PushData();
    }

    void OnLostFocus()
    {
        PushData();
    }

    void PushData()
    {
        engine.waypoints = waypointObjects;
    }

    void OnGUI()
    {
        Selection.activeGameObject = waypointObjects[waypointIndex];
        HeaderGUI();
        ButtonsGUI();
        WaypointGUI();
    }

    void HeaderGUI()
    {
        GUIStyle titleText = new GUIStyle();
        titleText.fontSize = 24;
        titleText.fontStyle = FontStyle.Bold;

        debugMode = GUI.Toggle(new Rect(455, 2.5f, 92, 16), debugMode, "Debug Mode ");

        GUI.Label(new Rect(170, 10, 400, 30), "Engine Editor", titleText);

        titleText.fontSize = 18;
        titleText.fontStyle = FontStyle.Normal;

        GUI.Label(new Rect(110, 40, 150, 20), "Jump to waypoint:", titleText);
        waypointIndex = EditorGUI.Popup(new Rect(300, 44, 100, 20), waypointIndex, waypointNames);

        GUI.Label(new Rect(109, 70, 150, 20), "Waypoint position: ", titleText);
        waypointObjects[waypointIndex].transform.position = EditorGUI.Vector3Field(new Rect(300, 74, 200, 20), GUIContent.none, waypointObjects[waypointIndex].transform.position);
        
        #region Debug Code
        if (debugMode)
        {
            GUI.BeginGroup(new Rect(0, 580, 150, 60));
            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveType != MovementType.WAIT)
            {
                GUI.Label(new Rect(0, 0, 150, 20), waypointObjects[waypointIndex].name);
                GUI.Label(new Rect(0, 20, 150, 20), "start: " + waypointObjects[waypointIndex].transform.position);
                GUI.Label(new Rect(0, 40, 150, 20), "goto: " + waypointObjects[waypointIndex + 1].transform.position);
            }
            GUI.EndGroup();

            GUI.BeginGroup(new Rect(360, 580, 150, 60));
            GUI.Label(new Rect(0, 0, 150, 20), "Number of waypoints: " + waypointObjects.Count);
            GUI.Label(new Rect(0, 20, 150, 20), "waypointIndex: " + waypointIndex);
            GUI.EndGroup();
        }
        #endregion
    }

    void ButtonsGUI()
    {
        Rect selectiveButtonsSpace = new Rect(20, 100, 510, 30);
        Rect creativeButtonSpace = new Rect(20, 500, 510, 40);

        #region Change Index Buttons
        GUI.BeginGroup(selectiveButtonsSpace);
            GUI.color = Color.yellow;
            if (waypointIndex > 0)
            {
                if (GUI.Button(new Rect(0, 0, 80, 30), "<< First"))
                    waypointIndex = 0;
                if (GUI.Button(new Rect(80, 0, 80, 30), "< Previous"))
                    waypointIndex--;
            }
            if (waypointIndex < waypointObjects.Count - 1)
            {
                if (GUI.Button(new Rect(340, 0, 80, 30), "Next >"))
                    waypointIndex++;
                if (GUI.Button(new Rect(420, 0, 80, 30), " Last >>"))
                    waypointIndex = waypointObjects.Count - 1;
            }
        GUI.EndGroup();
        #endregion

        #region Add/Remove Waypoints Buttons

        GUI.BeginGroup(creativeButtonSpace);

            
            #region Insert Before Button

            GUI.color = Color.green;
            if(GUI.Button(new Rect(0, 0, 120, 40), "< Insert before"))
            {
                waypointObjects.Insert(waypointIndex, CreateWaypointBeforeIndex(waypointIndex));
                waypointNames = RenameWaypoints();
            }

            #endregion

            #region Delete Waypoint Button

            GUI.color = Color.red;
            if (waypointObjects.Count > 1)
            {
                if (GUI.Button(new Rect(190, 0, 120, 40), "Delete waypoint"))
                {
                    if (waypointIndex == waypointObjects.Count - 1)
                    {
                        waypointIndex--;
                        waypointObjects[waypointObjects.Count - 2].GetComponent<WaypointScript>().moveData.endPos = waypointObjects[waypointObjects.Count - 2];
                        waypointObjects[waypointObjects.Count - 2].GetComponent<WaypointScript>().moveType = MovementType.WAIT;
                        DestroyImmediate(waypointObjects[waypointObjects.Count - 1]);
                        waypointObjects.RemoveAt(waypointObjects.Count - 1);
                    }
                    else
                    {
                        if (waypointIndex > 0)
                        {
                            waypointObjects[waypointIndex - 1].GetComponent<WaypointScript>().moveData.endPos = waypointObjects[waypointIndex + 1];
                        }
                        DestroyImmediate(waypointObjects[waypointIndex]);
                        waypointObjects.RemoveAt(waypointIndex);
                    }
                    waypointNames = RenameWaypoints();
                }
                if (waypointObjects.Count == 1 && waypointObjects[0].GetComponent<WaypointScript>().moveType != MovementType.WAIT)
                {
                    waypointObjects[0].GetComponent<WaypointScript>().moveData.startPos = waypointObjects[0];
                    waypointObjects[0].GetComponent<WaypointScript>().moveData.endPos = waypointObjects[0];
                    waypointObjects[0].GetComponent<WaypointScript>().moveType = MovementType.WAIT;
                }
            }

            #endregion

            #region Insert After Button

            if (waypointIndex < waypointObjects.Count - 1)
            {
                GUI.color = Color.green;
                if (GUI.Button(new Rect(385, 0, 120, 40), "Insert after >"))
                {
                    waypointObjects.Insert(waypointIndex + 1, CreateWaypointAfterIndex(waypointIndex));
                    waypointNames = RenameWaypoints();
                }
            }
            else
            {
                GUI.color = Color.cyan;
                if (GUI.Button(new Rect(385, 0, 120, 40), "Add Waypoint"))
                {
                    waypointObjects.Add(CreateWaypointAfterIndex(waypointIndex));
                    waypointNames = RenameWaypoints();
                }
            }

            #endregion

        GUI.EndGroup();

        #endregion

        GUI.color = Color.white;

        if (GUI.Button(new Rect(150, 580, 200, 40), "Done"))
        {
            PushData();
            window.Close();
        }        
    }

    void WaypointGUI()
    {
        Rect waypointDisplay = new Rect(25, 150, 640, 335);

        GUI.BeginGroup(waypointDisplay);

            #region GUI Display Vars
        Rect movementBoxArea = new Rect(0, 0, 240, 112);
        Rect facingBoxArea = new Rect(0, 120, 240, 215);
        Rect effectsBoxArea = new Rect(250, 0, 250, 335);

        GUI.Box(movementBoxArea, GUIContent.none);
        GUI.Box(facingBoxArea, GUIContent.none);
        GUI.Box(effectsBoxArea, GUIContent.none);

        float guiHeight = 16f;
        float labelX = 100f;
        float yAddOffset = 20f;
        float yOffset = 4;
        float floatFieldWidth = 30f;

        float movementGuiX = 108f;
        float movementFieldX = 165f;
        float facingGUIX = 80f;
        float facingFieldX = 165f;
        float effectGUIX = 100f;
        float effectFieldX = 170f;


        float minTime = 0;
        float maxDelay = 10;
        float maxMoveTime = 60;

        int minFocusPoints = 1;
        int maxFocusPoints = 10;

        int minEffects = 0;
        int maxEffects = 10;

        float minShakeIntensity = 0;
        float maxShakeIntensity = 2;
        

        GUIStyle boxTitle = new GUIStyle();
        boxTitle.fontSize = 16;
        boxTitle.fontStyle = FontStyle.Bold;
        #endregion

            #region Movements GUI
        GUI.BeginGroup(movementBoxArea);
                GUI.Label(new Rect(40, yOffset, labelX, guiHeight), "Movement Data", boxTitle);
                yOffset += yAddOffset + 4;
                if (waypointIndex != waypointObjects.Count - 1)
                {
                    GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Movement Type: ");
                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveType = (MovementType)EditorGUI.EnumPopup(new Rect(movementGuiX, yOffset, movementGuiX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveType);
                    yOffset += yAddOffset;
                }

                switch (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveType)
                {
                    case(MovementType.STRAIGHT_LINE):
                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Move Delay: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = GUI.HorizontalSlider(new Rect(movementGuiX, yOffset, movementGuiX / 2, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay, minTime, maxDelay);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = EditorGUI.FloatField(new Rect(movementFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay > maxDelay)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = maxDelay;
                        GUI.Label(new Rect(movementFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Move Time: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = GUI.HorizontalSlider(new Rect(movementGuiX, yOffset, movementGuiX / 2, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime, minTime, maxMoveTime);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = EditorGUI.FloatField(new Rect(movementFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime > maxMoveTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = maxMoveTime;
                        GUI.Label(new Rect(movementFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");

                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint != null)
                        {
                            DestroyImmediate(waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint);
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint = null;
                        }
                        break;
                    case(MovementType.BEZIER_CURVE):
                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Move Delay: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = GUI.HorizontalSlider(new Rect(movementGuiX, yOffset, movementGuiX / 2, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay, minTime, maxDelay);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = EditorGUI.FloatField(new Rect(movementFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay > maxDelay)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = maxDelay;
                        GUI.Label(new Rect(movementFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Move Time: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = GUI.HorizontalSlider(new Rect(movementGuiX, yOffset, movementGuiX / 2, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime, minTime, maxMoveTime);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = EditorGUI.FloatField(new Rect(movementFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime > maxMoveTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = maxMoveTime;
                        GUI.Label(new Rect(movementFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Curve point pos.");
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint == null)
                        {
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint = CreateCurvePoint();
                        }
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint.transform.position = EditorGUI.Vector3Field(new Rect(movementGuiX, yOffset, movementGuiX + 15, guiHeight), GUIContent.none, waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint.transform.position);
                        
                        break;
                    default:

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Wait Time: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = GUI.HorizontalSlider(new Rect(movementGuiX, yOffset, movementGuiX / 2, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime, minTime, maxMoveTime);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = EditorGUI.FloatField(new Rect(movementFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime > maxMoveTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = maxMoveTime;
                        GUI.Label(new Rect(movementFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");

                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint != null)
                        {
                            DestroyImmediate(waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint);
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint = null;
                        }

                        break;
                }
            GUI.EndGroup();
            #endregion

            #region Facings GUI
            yOffset = 4;
            GUI.BeginGroup(facingBoxArea);
                GUI.Label(new Rect(50, yOffset, labelX, guiHeight), "Facing Data", boxTitle);
                yOffset += yAddOffset + 4;
                GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Facing type: ");
                waypointObjects[waypointIndex].GetComponent<WaypointScript>().facing = (FacingType)EditorGUI.EnumPopup(new Rect(facingGUIX, yOffset, 135, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().facing);
                yOffset += yAddOffset;

                switch (waypointObjects[waypointIndex].GetComponent<WaypointScript>().facing)
                {
                    case(FacingType.LOOK_AND_RETURN):
                                                
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = 1;
                        ChangeFocusPointsLists(waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints);

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Face Delay: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay, minTime, maxDelay);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay > maxDelay)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Pan Time: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0], minTime, maxDelay);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0]);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] > maxDelay)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Focus Time: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0] = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0], minTime, maxDelay);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0] = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0]);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0] < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0] = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0] > maxDelay)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0] = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Return Time: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime, minTime, maxDelay);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime > maxDelay)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Focus point pos.");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[0].transform.position = EditorGUI.Vector3Field(new Rect(labelX, yOffset, facingGUIX + facingGUIX / 2, guiHeight), GUIContent.none, waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[0].transform.position);

                        

                        break;
                    case(FacingType.FORCED_DIRECTION):

                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = 1;
                        ChangeFocusPointsLists(waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints);


                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Face Delay: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay, minTime, maxDelay);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay > maxDelay)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Pan Time: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0], minTime, maxDelay);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0]);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] > maxDelay)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Focus point pos.");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[0].transform.position = EditorGUI.Vector3Field(new Rect(labelX, yOffset, facingGUIX + facingGUIX / 2, guiHeight), GUIContent.none, waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[0].transform.position);

                        

                        break;
                    case(FacingType.LOOK_CHAIN):
                        
                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Face Delay: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay, minTime, maxDelay);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay > maxDelay)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Focus points: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = (int)GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints, minFocusPoints, maxFocusPoints);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = EditorGUI.IntField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints < minFocusPoints)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = minFocusPoints;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints > maxFocusPoints)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = maxFocusPoints;

                        ChangeFocusPointsLists(waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints);
                        if (focusPointsIndex > 0 && focusPointsIndex > waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints - 1)
                            focusPointsIndex--;

                        focusPointNames = new string[waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints];
                        for (int i = 0; i < waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints; i++)
                            focusPointNames[i] = waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[i].name;
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(80, yOffset, labelX, guiHeight), "Focus point " + (focusPointsIndex + 1));
                        yOffset += yAddOffset;
                        GUI.BeginGroup(new Rect(0, yOffset, 240, guiHeight));
                            if (focusPointsIndex > 0)
                            {
                                if (GUI.Button(new Rect(0, 0, 40, guiHeight), "<<"))
                                    focusPointsIndex = 0;
                                if (GUI.Button(new Rect(40, 0, 40, guiHeight), "<"))
                                    focusPointsIndex--;
                            }
                            focusPointsIndex = EditorGUI.Popup(new Rect(80, 0, 80, guiHeight), focusPointsIndex, focusPointNames);
                            if (focusPointsIndex < waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints - 1)
                            {
                                if (GUI.Button(new Rect(160, 0, 40, guiHeight), ">"))
                                    focusPointsIndex++;
                                if (GUI.Button(new Rect(200, 0, 40, guiHeight), ">>"))
                                    focusPointsIndex = waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints - 1;
                            }
                        GUI.EndGroup();
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(10, yOffset, labelX, guiHeight), "Pan Time: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusPointsIndex] = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusPointsIndex], minTime, maxDelay);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusPointsIndex] = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusPointsIndex]);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusPointsIndex] < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusPointsIndex] = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusPointsIndex] > maxDelay)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusPointsIndex] = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(10, yOffset, labelX, guiHeight), "Focus Time: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusPointsIndex] = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusPointsIndex], minTime, maxDelay);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusPointsIndex] = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusPointsIndex]);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusPointsIndex] < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusPointsIndex] = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusPointsIndex] > maxDelay)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusPointsIndex] = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;
                        
                        GUI.Label(new Rect(10, yOffset, labelX, guiHeight), "Focus point pos.");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[focusPointsIndex].transform.position = EditorGUI.Vector3Field(new Rect(labelX + 10, yOffset, facingGUIX + facingGUIX / 2, guiHeight), GUIContent.none, waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[focusPointsIndex].transform.position);
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Return Time: ");
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime, minTime, maxDelay);
                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime);
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime < minTime)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = minTime;
                        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime > maxDelay)
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;
                        
                        break;
                    default:  

                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = 0;
                        ChangeFocusPointsLists(waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints);

                        GUIStyle italic = new GUIStyle();
                        italic.fontStyle = FontStyle.Italic;
                        GUI.Label(new Rect(20, yOffset, labelX * 2, guiHeight * 2), "No parameters necessary\nfor " + (waypointObjects[waypointIndex].GetComponent<WaypointScript>().facing == FacingType.WAIT ? "no " : "Free Look ") + "facing", italic); 
                        
                        break;
                }            
            GUI.EndGroup();
            #endregion

            #region Effects GUI
            yOffset = 4;
            GUI.BeginGroup(effectsBoxArea);
                GUI.Label(new Rect(50, yOffset, labelX, guiHeight), "Effects Data", boxTitle);
                yOffset += yAddOffset + 4;
                GUI.Label(new Rect(0, yOffset, labelX + 20, guiHeight), "Number of effects:");
                waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects = (int)GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, 75, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects, minEffects, maxEffects);
                waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects = EditorGUI.IntField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects);
                if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects < minEffects)
                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects = minEffects;
                if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects > maxEffects)
                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects = maxEffects;               
                
                ChangeEffectsList(waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects);
                if (effectsIndex > 0 && effectsIndex > waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects - 1)
                    effectsIndex--;
                effectNames = new string[waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects];
                for (int i = 0; i < waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects; i++)
                {
                    switch (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[i].effectType)
                    {                        
                        case(EffectType.CAMERA_SHAKE):
                            effectNames[i] = "Shake ";
                            break;
                        case(EffectType.CAMERA_FADE):
                            switch (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[i].fadeType)
                            {
                                case(FadeType.FADE_IN):
                                    effectNames[i] = "Fade In ";
                                    break;
                                case(FadeType.FADE_OUT):
                                    effectNames[i] = "Fade Out ";
                                    break;
                                case(FadeType.FADE_IN_THEN_OUT):
                                    effectNames[i] = "Fade In->Out ";
                                    break;
                                case(FadeType.FADE_OUT_THEN_IN):
                                    effectNames[i] = "Fade Out->In ";
                                    break;
                            }
                            break;
                        case(EffectType.CAMERA_SPLATTER):
                            effectNames[i] = "Splatter ";
                            break;
                        default:
                            effectNames[i] = "Wait ";
                            break;
                    }
                }
                yOffset += yAddOffset;
                if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects > 0)
                {
                    GUI.Label(new Rect(90, yOffset, labelX, guiHeight), "Effect " + (effectsIndex + 1));
                    yOffset += yAddOffset;
                    GUI.BeginGroup(new Rect(0, yOffset, 250, guiHeight));
                    if (effectsIndex > 0)
                    {
                        if (GUI.Button(new Rect(5, 0, 40, guiHeight), "<<"))
                            effectsIndex = 0;
                        if (GUI.Button(new Rect(45, 0, 40, guiHeight), "<"))
                        {
                            effectsIndex--;
                        }
                    }
                    effectsIndex = EditorGUI.Popup(new Rect(85, 0, 80, guiHeight), effectsIndex, effectNames);
                    if (effectsIndex < waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects - 1)
                    {
                        if (GUI.Button(new Rect(165, 0, 40, guiHeight), ">"))
                            effectsIndex++;
                        if (GUI.Button(new Rect(205, 0, 40, guiHeight), ">>"))
                            effectsIndex = waypointObjects[waypointIndex].GetComponent<WaypointScript>().numEffects - 1;
                    }
                    GUI.EndGroup();
                    yOffset += yAddOffset;

                    GUI.Label(new Rect(10, yOffset, labelX, guiHeight), "Effect type: ");
                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectType = (EffectType)EditorGUI.EnumPopup(new Rect(effectGUIX, yOffset, effectGUIX + 35, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectType);
                    yOffset += yAddOffset;

                    switch (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectType)
                    {
                        case(EffectType.CAMERA_SHAKE):
                            GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Shake Delay: ");
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay, minTime, maxDelay);
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay);
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay < minTime)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay = minTime;
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay > maxDelay)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay = maxDelay;
                            yOffset += yAddOffset;

                            GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Shake Time: ");
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration, minTime, maxMoveTime);
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration);
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration < minTime)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration = minTime;
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration > maxMoveTime)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration = maxMoveTime;
                            yOffset += yAddOffset;

                            GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Shake Intensity: ");
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].shakeIntensity = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].shakeIntensity, minShakeIntensity, maxShakeIntensity);
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].shakeIntensity = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].shakeIntensity);
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].shakeIntensity < minShakeIntensity)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].shakeIntensity = minShakeIntensity;
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].shakeIntensity > maxShakeIntensity)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].shakeIntensity = maxShakeIntensity;

                            break;
                        case(EffectType.CAMERA_FADE):
                            GUI.Label(new Rect(10, yOffset, labelX, guiHeight), "Fade Type: ");
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeType = (FadeType)EditorGUI.EnumPopup(new Rect(effectGUIX, yOffset, effectGUIX + 35, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeType);
                            yOffset += yAddOffset;

                            GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Fade Delay: ");
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay, minTime, maxDelay);
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay);
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay < minTime)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay = minTime;
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay > maxDelay)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay = maxDelay;
                            yOffset += yAddOffset;

                            
                            switch(waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeType)
                            {
                                case(FadeType.FADE_IN):
                                    GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Fade Time: ");
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime, minTime, maxMoveTime);
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime);
                                    if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime < minTime)
                                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = minTime;
                                    if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime > maxMoveTime)
                                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = maxMoveTime;
                                    yOffset += yAddOffset;
                                    break;
                                case(FadeType.FADE_OUT):
                                    GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Fade Time: ");
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime, minTime, maxMoveTime);
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime);
                                    if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime < minTime)
                                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = minTime;
                                    if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime > maxMoveTime)
                                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = maxMoveTime;
                                    yOffset += yAddOffset;
                                    break;
                                case(FadeType.FADE_IN_THEN_OUT):
                                    GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Fade In Time: ");
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime, minTime, maxMoveTime);
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime);
                                    if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime < minTime)
                                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = minTime;
                                    if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime > maxMoveTime)
                                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = maxMoveTime;
                                    yOffset += yAddOffset;

                                    GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Fade Out Time: ");
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime, minTime, maxMoveTime);
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime);
                                    if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime < minTime)
                                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = minTime;
                                    if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime > maxMoveTime)
                                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = maxMoveTime;
                                    yOffset += yAddOffset;
                                    break;
                                case(FadeType.FADE_OUT_THEN_IN):
                                    GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Fade Out Time: ");
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime, minTime, maxMoveTime);
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime);
                                    if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime < minTime)
                                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = minTime;
                                    if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime > maxMoveTime)
                                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = maxMoveTime;
                                    yOffset += yAddOffset;

                                    GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Fade In Time: ");
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime, minTime, maxMoveTime);
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime);
                                    if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime < minTime)
                                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = minTime;
                                    if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime > maxMoveTime)
                                        waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = maxMoveTime;
                                    yOffset += yAddOffset;
                                    break;                                       
                            }
                            GUI.Label(new Rect(10, yOffset, labelX, guiHeight), "Fade Color: ");
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeColor = EditorGUI.ColorField(new Rect(effectGUIX, yOffset, effectGUIX + 35, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeColor);
                            break;

                        case(EffectType.CAMERA_SPLATTER):
                            GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Splatter Delay: ");
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay, minTime, maxDelay);
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay);
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay < minTime)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay = minTime;
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay > maxDelay)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDelay = maxDelay;
                            yOffset += yAddOffset;

                            GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Linger Time: ");
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration, minTime, maxMoveTime);
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration);
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration < minTime)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration = minTime;
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration > maxMoveTime)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration = maxMoveTime;
                            yOffset += yAddOffset;

                            GUI.Label(new Rect(10, yOffset, labelX, guiHeight), "Fade splatter? ");
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].splatterFade = GUI.Toggle(new Rect(effectGUIX + 15, yOffset, effectGUIX, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].splatterFade, GUIContent.none);
                            yOffset += yAddOffset;

                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].splatterFade)
                            {
                                GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Fade In Time: ");
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime, minTime, maxMoveTime);
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime);
                                if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime < minTime)
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = minTime;
                                if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime > maxMoveTime)
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeInTime = maxMoveTime;
                                yOffset += yAddOffset;

                                GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Fade Out Time: ");
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime, minTime, maxMoveTime);
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime);
                                if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime < minTime)
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = minTime;
                                if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime > maxMoveTime)
                                    waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].fadeOutTime = maxMoveTime;
                                yOffset += yAddOffset;
                            }
                            break;
                        default:
                            GUI.Label(new Rect(10, yOffset, labelX + 30, guiHeight), "Wait Time: ");
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration = GUI.HorizontalSlider(new Rect(effectGUIX + 20, yOffset, effectGUIX / 2 + effectGUIX / 4, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration, minTime, maxMoveTime);
                            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration = EditorGUI.FloatField(new Rect(effectFieldX + 30, yOffset, floatFieldWidth, guiHeight), waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration);
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration < minTime)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration = minTime;
                            if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration > maxMoveTime)
                                waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData[effectsIndex].effectDuration = maxMoveTime;
                            yOffset += yAddOffset;

                            break;
                    }
                }

                
            GUI.EndGroup();
            #endregion

        GUI.EndGroup();
    }

    GameObject CreateWaypointBeforeIndex(int insertIndex)
    {
        GameObject waypoint = CreateWaypoint((insertIndex + 1));
        Debug.Log("Creating new " + waypoint.name);

        #region New Waypoint Transform assign
        // Assign the new waypoint transform
        // If inserting after last waypoint, place 2 units ahead of last waypoint
        // Otherwise, place between the current and next waypoints
        if (insertIndex > 0)
        {
            Debug.Log("Point 1: " + waypointObjects[insertIndex - 1].transform.position);
            Debug.Log("Point 2: " + waypointObjects[insertIndex].transform.position);
            Debug.Log("Placing new point between " + waypointObjects[insertIndex - 1].transform.position + " and " + waypointObjects[insertIndex].transform.position + "\n---> " + WaypointsMidway(waypointObjects[insertIndex - 1].transform.position, waypointObjects[insertIndex].transform.position));
            waypoint.transform.position = WaypointsMidway(waypointObjects[insertIndex - 1].transform.position, waypointObjects[insertIndex].transform.position);
        }
        else
        {
            Debug.Log("Placing new point at start ---> " + (waypointObjects[0].transform.position + (Vector3.back * 2)));
            waypoint.transform.position = waypointObjects[0].transform.position + (Vector3.back * 2);
        }
        #endregion

        #region Add New Waypoint to Rail Engine

        // Link the waypoint into the engine waypoints list

        // If inserting in the middle of the list,
        //    The previous point movement becomes a straight line if it's a wait with the waypoint as its endpoint,
        //    and the waypoint is also a straight line movement with the next waypoint as its endpoint

        // If inserting at the end of the list,
        //    The previous point movement becomes a straight line if it's a wait with the waypoint as its endpoint,
        //    and the waypoint is a wait movement.
        if (insertIndex > 0)
        {
            if (waypointObjects[insertIndex - 1].GetComponent<WaypointScript>().moveType == MovementType.WAIT)
            {
                waypointObjects[insertIndex - 1].GetComponent<WaypointScript>().moveType = MovementType.STRAIGHT_LINE;
                Debug.Log("The waypoint at " + waypointObjects[insertIndex - 1].transform.position + " now has a straight line movement");
            }
            waypointObjects[insertIndex - 1].GetComponent<WaypointScript>().moveData.endPos = waypoint;
            Debug.Log("The waypoint at " + waypointObjects[insertIndex - 1].transform.position + " now goes to new point @ " + waypoint.transform.position);

            waypoint.GetComponent<WaypointScript>().moveData.startPos = waypoint;
            waypoint.GetComponent<WaypointScript>().moveData.endPos = waypointObjects[insertIndex];
            waypoint.GetComponent<WaypointScript>().moveType = MovementType.STRAIGHT_LINE;
            Debug.Log("new point @ " + waypoint.transform.position + " goes to " + waypointObjects[insertIndex].transform.position);
        }
        else
        {
            waypoint.GetComponent<WaypointScript>().moveType = MovementType.STRAIGHT_LINE;
            waypoint.GetComponent<WaypointScript>().moveData.startPos = waypoint;
            waypoint.GetComponent<WaypointScript>().moveData.endPos = waypointObjects[0];
            Debug.Log("new point @ " + waypoint.transform.position + " goes to " + waypointObjects[0].transform.position);
        }
        #endregion

        return waypoint;
    }

    GameObject CreateWaypointAfterIndex(int insertIndex)
    {
        GameObject waypoint = CreateWaypoint((insertIndex + 2));
        Debug.Log("Creating new " + waypoint.name);

        #region New Waypoint Transform assign
        // Assign the new waypoint transform
        // If inserting after last waypoint, place 2 units ahead of last waypoint
        // Otherwise, place between the current and next waypoints
        if (insertIndex < waypointObjects.Count - 1)
        {
            Debug.Log("Point 1: " + waypointObjects[insertIndex].transform.position);
            Debug.Log("Point 2: " + waypointObjects[insertIndex + 1].transform.position);
            Debug.Log("Placing new point between " + waypointObjects[insertIndex].transform.position + " and " + waypointObjects[insertIndex + 1].transform.position + "\n---> " + WaypointsMidway(waypointObjects[insertIndex].transform.position, waypointObjects[insertIndex + 1].transform.position));
            waypoint.transform.position = WaypointsMidway(waypointObjects[insertIndex].transform.position, waypointObjects[insertIndex + 1].transform.position);
        }
        else
        {
            Debug.Log("Placing new point at end ---> " + (waypointObjects[waypointObjects.Count - 1].transform.position + (Vector3.forward * 2)));
            waypoint.transform.position = waypointObjects[waypointObjects.Count - 1].transform.position + (Vector3.forward * 2);
        }
        #endregion

        #region Add New Waypoint to Rail Engine

        // Link the waypoint into the engine waypoints list

        // If inserting in the middle of the list,
        //    The previous point movement becomes a straight line if it's a wait with the waypoint as its endpoint,
        //    and the waypoint is also a straight line movement with the next waypoint as its endpoint

        // If inserting at the end of the list,
        //    The previous point movement becomes a straight line if it's a wait with the waypoint as its endpoint,
        //    and the waypoint is a wait movement.

        if (insertIndex < waypointObjects.Count - 1)
        {
            if (waypointObjects[insertIndex + 1].GetComponent<WaypointScript>().moveType == MovementType.WAIT && waypointObjects.IndexOf(waypointObjects[insertIndex + 1]) != (waypointObjects.Count - 1))
            {
                waypointObjects[insertIndex + 1].GetComponent<WaypointScript>().moveType = MovementType.STRAIGHT_LINE;
                Debug.Log("The waypoint at " + waypointObjects[insertIndex].transform.position + " now has a straight line movement");
            }
            waypointObjects[insertIndex].GetComponent<WaypointScript>().moveData.endPos = waypoint;
            Debug.Log("The waypoint at " + waypointObjects[insertIndex].transform.position + " now goes to new point @ " + waypoint.transform.position);

            waypoint.GetComponent<WaypointScript>().moveData.startPos = waypoint;
            waypoint.GetComponent<WaypointScript>().moveData.endPos = waypointObjects[insertIndex + 1];
            waypoint.GetComponent<WaypointScript>().moveType = MovementType.STRAIGHT_LINE;
            Debug.Log("new point @ " + waypoint.transform.position + " goes to " + waypointObjects[insertIndex + 1].transform.position);

        }
        else
        {
            if (waypointObjects[insertIndex].GetComponent<WaypointScript>().moveType == MovementType.WAIT)
            {
                waypointObjects[insertIndex].GetComponent<WaypointScript>().moveType = MovementType.STRAIGHT_LINE;
                Debug.Log("The waypoint at " + waypointObjects[insertIndex].transform.position + " now has a straight line movement");
            }
            waypointObjects[insertIndex].GetComponent<WaypointScript>().moveData.endPos = waypoint;
            if (insertIndex - 1 > 0)
                Debug.Log("The waypoint at " + waypointObjects[insertIndex - 1].transform.position + " now goes to new point @ " + waypoint.transform.position);
            else
                Debug.Log("The waypoint at " + waypointObjects[0].transform.position + " now goes to new point @ " + waypoint.transform.position);

            waypoint.GetComponent<WaypointScript>().moveData.startPos = waypoint;
            waypoint.GetComponent<WaypointScript>().moveData.endPos = waypoint;
            Debug.Log("New point is end of path");
        }

        #endregion

        return waypoint;
    }

    GameObject CreateWaypoint(int index)
    {
        GameObject waypoint = new GameObject();
        waypoint.name = "Waypoint " + (index);
        waypoint.tag = WaypointEnumValues.WAYPOINT_TAG;
        waypoint.transform.parent = engine.gameObject.transform;
        waypoint.AddComponent<WaypointScript>();
        waypoint.GetComponent<WaypointScript>().InitializeWaypoint();
        waypoint.AddComponent<WaypointGizmo>();

        return waypoint;
    }

    Vector3 WaypointsMidway(Vector3 waypointBefore, Vector3 waypointAfter)
    {
        float midwayX = (waypointBefore.x + waypointAfter.x) / 2f;
        float midwayY = (waypointBefore.y + waypointAfter.y) / 2f;
        float midwayZ = (waypointBefore.z + waypointAfter.z) / 2f;

        return new Vector3(midwayX, midwayY, midwayZ);
    }

    string[] RenameWaypoints()
    {
        string[] newNamesList = new string[waypointObjects.Count];
        for (int i = 0; i < waypointObjects.Count; i++)
        {
            waypointObjects[i].name = "Waypoint " + (i + 1);
            newNamesList[i] = waypointObjects[i].name;
        }

        return newNamesList;
    }

    void ChangeFocusPointsLists(int numFocusPoints)
    {
        // Make sure all three lists exist
        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints == null)
            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints = new List<GameObject>();
        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes == null)
            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes = new List<float>();
        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes == null)
            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes = new List<float>();

        if (numFocusPoints > waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count)
            AddFocusPoints(numFocusPoints);
        else if (numFocusPoints < waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count)
            DeleteFocusPoints(numFocusPoints);       
    }

    void AddFocusPoints(int numFocusPoints)
    {
        for (int i = waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count; i < numFocusPoints; i++)
        {
            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Add(CreateFocusPoint(("Focus " + (waypointIndex + 1) + "." + (i + 1)), i - 1));
            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes.Add(0);
            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes.Add(0);
        }        
    }

    void DeleteFocusPoints(int numFocusPoints)
    {

        for (int i = waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count; i > numFocusPoints; i--)
        {
            DestroyImmediate(waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count - 1]);
            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.RemoveAt(waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count - 1);
            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes.RemoveAt(waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count - 1);
            waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes.RemoveAt(waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count - 1);
        }
    }

    void ChangeEffectsList(int numEffects)
    {
        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData == null)
            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData = new List<EffectsScript>();

        if (numEffects > waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData.Count)
            AddEffects(numEffects);
        else if (numEffects < waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData.Count)
            DeleteEffects(numEffects);
    }

    void AddEffects(int numEffects)
    {
        for (int i = waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData.Count; i < numEffects; i++)
            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData.Add(new EffectsScript());
    }

    void DeleteEffects(int numEffects)
    {
        for (int i = waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData.Count; i > numEffects; i--)
            waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData.RemoveAt(waypointObjects[waypointIndex].GetComponent<WaypointScript>().effectsData.Count - 1);
    }    

    GameObject CreateCurvePoint()
    {
        GameObject curvePoint = new GameObject();
        curvePoint.name = "Curve " + (waypointIndex + 1);
        curvePoint.tag = WaypointEnumValues.WAYPOINT_TAG;
        curvePoint.transform.parent = waypointObjects[waypointIndex].transform;
        curvePoint.transform.localPosition = Vector3.forward + (Vector3.left) * 2;
        curvePoint.AddComponent<WaypointScript>();
        curvePoint.GetComponent<WaypointScript>().waypointType = WaypointType.CURVE;
        curvePoint.AddComponent<WaypointGizmo>();

        return curvePoint;
    }

    GameObject CreateFocusPoint(string name, int previousFocusPointIndex)
    {
        GameObject focusPoint = new GameObject();
        focusPoint.name = name;
        focusPoint.tag = WaypointEnumValues.WAYPOINT_TAG;
        focusPoint.transform.parent = waypointObjects[waypointIndex].transform;
        if (waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints < 2)
            focusPoint.transform.localPosition = (Vector3.one * 2) + (Vector3.left * 4);
        else
            focusPoint.transform.localPosition = waypointObjects[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[previousFocusPointIndex].transform.position + (Vector3.right * 2);
        focusPoint.AddComponent<WaypointScript>();
        focusPoint.GetComponent<WaypointScript>().waypointType = WaypointType.FOCUS;
        focusPoint.AddComponent<WaypointGizmo>();

        return focusPoint;
    }
}
