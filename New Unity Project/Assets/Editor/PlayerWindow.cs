﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerWindow : EditorWindow
{
    const int MIN_NUM_WEAPONS = 1;
    const int MAX_NUM_WEAPONS = 10;
    const float MIN_PROJECTILE_FORCE = 1000;
    const float MAX_PROJECTILE_FORCE = 10000;
    const float MIN_DAMAGE_DEALT = 10;
    const float MAX_DAMAGE_DEALT = 200;
    const float MIN_CLIP_SIZE = 1;
    const float MAX_CLIP_SIZE = 100;
    const float MIN_COOLDOWN_RELOAD = 0;
    const float MAX_COOLDOWN_RELOAD = 20;

    GameObject scenePlayer;
    PlayerScript windowPlayer;
    string[] weaponNames;

    public Button weaponSelectionBtn;
    public Button insertWeaponBtn;
    public Button deleteWeaponBtn;

    public Color selectColor = Color.yellow;
    public Color insertColor = Color.green;
    public Color deleteColor = Color.red;    
    
    float guiHeight = 16f;
    int numWeapons;
    int weaponPointer = 0;

    static PlayerWindow window;

    void OnEnable()
    {
        InitializeWindowData();
    }

    //[MenuItem("Rail Engine/Edit Player Weapons")]
    public static void OpenPlayerEditorWindow()
    {
        window = (PlayerWindow)EditorWindow.GetWindow(typeof(PlayerWindow));
        window.titleContent.text = "Weapons";
        window.minSize = new Vector2(250, 400);
        window.maxSize = window.minSize;
        window.Show();
    }

    void InitializeWindowData()
    {
        scenePlayer = GameObject.FindGameObjectWithTag(WaypointEnumValues.PLAYER_TAG);
        windowPlayer = scenePlayer.GetComponent<PlayerScript>();

        if (windowPlayer.editorWeapons == null)
        {
            windowPlayer.editorWeapons = new WeaponScript[1];
        }
        if (windowPlayer.editorWeapons.Length < 1)
        {
            GameObject dummy = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Dynamic Assets/Resources/Prefabs/Cannonball.prefab", typeof(GameObject));
            windowPlayer.editorWeapons[0] = new WeaponScript("Null projectile", dummy, 2000, 100, 1, 0.5f, 1, DamageType.NONE);
            numWeapons = 1;
        }
        else
        {
            numWeapons = windowPlayer.editorWeapons.Length;
        }

        weaponNames = new string[numWeapons];
        for (int i = 0; i < numWeapons; i++)
        {
            weaponNames[i] = windowPlayer.editorWeapons[i].name;
        }

        Debug.Log(scenePlayer.tag + " captured in window");
    }

    void OnFocus()
    {
        InitializeWindowData();
        weaponPointer = 0;
    }

    void OnLostFocus()
    {
        PushData();
    }

    void OnDestroy()
    {
        PushData();
    }

    void PushData()
    {
        PlayerScript.PlayerWeaponsAssign(windowPlayer, scenePlayer.GetComponent<PlayerScript>());
        //Undo.RecordObject(windowPlayer, "Player weapons list changed");
    }

    void OnGUI()
    {
        HeaderGUI();
        ButtonsGUI();
        WeaponsGUI();
        
    }

    void HeaderGUI()
    {
        GUIStyle titleText = new GUIStyle();
        titleText.fontSize = 18;
        titleText.fontStyle = FontStyle.Bold;

        EditorGUI.LabelField(new Rect(0, 5, 250, guiHeight), "  Player weapons editor", titleText);
        EditorGUI.LabelField(new Rect(20, 40, 106, guiHeight), "Jump to weapon: ");
        weaponPointer = EditorGUI.Popup(new Rect(130, 40, 100, guiHeight), weaponPointer, weaponNames);
    }

    void ButtonsGUI()
    {
        Rect selectiveButtonsSpace = new Rect(20, 60, 210, 20);
        Rect creativeButtonsSpace = new Rect(20, 255, 210, 20);

        #region Change Pointer Buttons
        GUI.BeginGroup(selectiveButtonsSpace);
            GUI.color = Color.yellow;
            if (weaponPointer != 0)
            {
                if (GUI.Button(new Rect(0, 0, 55, 20), "<< First"))
                    weaponPointer = 0;
                if (GUI.Button(new Rect(55, 0, 50, 20), "< Prev"))
                    weaponPointer--;

            }
            if (weaponPointer != numWeapons - 1)
            {
                if (GUI.Button(new Rect(105, 0, 50, 20), "Next >"))
                    weaponPointer++;
                if (GUI.Button(new Rect(155, 0, 55, 20), "Last >>"))
                    weaponPointer = numWeapons - 1;
            }
        GUI.EndGroup();
        #endregion

        #region Add/Remove Weapons Buttons
        GUI.BeginGroup(creativeButtonsSpace);
            if (numWeapons < MAX_NUM_WEAPONS)
            {
                GUI.color = Color.green;
                if (GUI.Button(new Rect(0, 0, 80, 20), "< Insert"))
                {
                    numWeapons++;
                    windowPlayer.editorWeapons = windowPlayer.IncreaseWeaponsArraySize(numWeapons);
                    weaponNames = IncreaseNamesArraySize(numWeapons);
                }
            }
            if (numWeapons > 1)
            {
                GUI.color = Color.red;
                if (GUI.Button(new Rect(80, 0, 50, 20), "Delete"))
                {
                    if (weaponPointer == numWeapons - 1)
                    {
                        weaponPointer--;
                        weaponNames = DecreaseNamesArraySize(numWeapons - 1, numWeapons - 1);
                        windowPlayer.editorWeapons = windowPlayer.DecreaseWeaponsArraySize(numWeapons - 1, numWeapons - 1);
                    }
                    else
                    {
                        weaponNames = DecreaseNamesArraySize(numWeapons - 1, weaponPointer);
                        windowPlayer.editorWeapons = windowPlayer.DecreaseWeaponsArraySize(numWeapons - 1, weaponPointer);
                    }

                    numWeapons--;
                }
            }
            if (numWeapons < MAX_NUM_WEAPONS)
            {
                if (weaponPointer < numWeapons - 1)
                {
                    GUI.color = Color.green;
                    if (GUI.Button(new Rect(130, 0, 80, 20), "Insert >"))
                    {
                        numWeapons++;
                        windowPlayer.editorWeapons = windowPlayer.IncreaseWeaponsArraySize(numWeapons);
                        weaponNames = IncreaseNamesArraySize(numWeapons);
                    }
                }
                else
                {
                    GUI.color = Color.cyan;
                    if (GUI.Button(new Rect(130, 0, 80, 20), "Add"))
                    {
                        numWeapons++;
                        windowPlayer.editorWeapons = windowPlayer.IncreaseWeaponsArraySize(numWeapons);
                        weaponNames = IncreaseNamesArraySize(numWeapons);
                    }
                }
            }
        
            GUI.color = Color.white;
        GUI.EndGroup();
        #endregion

        if (GUI.Button(new Rect(20, 280, 210, 20), "Done"))
        {
            PushData();
            window.Close();
        }

        GUI.Label(new Rect(20, 300, 210, 20), "Number of weapons: " + numWeapons);
        GUI.Label(new Rect(20, 320, 210, 20), "Weapons list count: " + windowPlayer.editorWeapons.Length);
        GUI.Label(new Rect(20, 340, 210, 20), "weaponPointer current location: " + weaponPointer);
        
        
    }

    void WeaponsGUI()
    {        
        float labelX = 100f;
        float guiX = 102f;
        float fieldX = 162;
        float fieldWidth = 40;
        float yAddOffset = 20f;
        float yOffset = 4;

        Rect weaponsBoxRect = new Rect(20, 85, 210, 164);
        GUI.Box(weaponsBoxRect, GUIContent.none);

        GUI.BeginGroup(weaponsBoxRect);
            GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Weapon name:     ");
            if (windowPlayer.editorWeapons[weaponPointer].name.Equals(""))
                GUI.color = Color.red;
            windowPlayer.editorWeapons[weaponPointer].name = GUI.TextField(new Rect(guiX, yOffset, guiX, guiHeight), windowPlayer.editorWeapons[weaponPointer].name);
            weaponNames[weaponPointer] = windowPlayer.editorWeapons[weaponPointer].name;
            GUI.color = Color.white;
            yOffset += yAddOffset;

            GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Projectile: ");
            if (windowPlayer.editorWeapons[weaponPointer].projectile == null)
                GUI.color = Color.red;
            windowPlayer.editorWeapons[weaponPointer].projectile = (GameObject)EditorGUI.ObjectField(new Rect(guiX, yOffset, guiX, guiHeight), windowPlayer.editorWeapons[weaponPointer].projectile, typeof(GameObject), false);
            GUI.color = Color.white;
            yOffset += yAddOffset;

            // Slider rect: (guiX - 10, yOffset, guiX/2, guiY)
            // Field rect:  (162, yOffset, 40, guiY)

            GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Force: ");
            windowPlayer.editorWeapons[weaponPointer].bulletForce = GUI.HorizontalSlider(new Rect(guiX, yOffset, guiX/2, guiHeight), windowPlayer.editorWeapons[weaponPointer].bulletForce, MIN_PROJECTILE_FORCE, MAX_PROJECTILE_FORCE);
            windowPlayer.editorWeapons[weaponPointer].bulletForce = EditorGUI.FloatField(new Rect(fieldX, yOffset, fieldWidth, guiHeight), windowPlayer.editorWeapons[weaponPointer].bulletForce);
            if (windowPlayer.editorWeapons[weaponPointer].bulletForce < MIN_PROJECTILE_FORCE)
                windowPlayer.editorWeapons[weaponPointer].bulletForce = MIN_PROJECTILE_FORCE;
            if (windowPlayer.editorWeapons[weaponPointer].bulletForce > MAX_PROJECTILE_FORCE)
                windowPlayer.editorWeapons[weaponPointer].bulletForce = MAX_PROJECTILE_FORCE;
            yOffset += yAddOffset;

            GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Damage:");
            windowPlayer.editorWeapons[weaponPointer].damage = (int)GUI.HorizontalSlider(new Rect(guiX, yOffset, guiX/2, guiHeight), windowPlayer.editorWeapons[weaponPointer].damage, MIN_DAMAGE_DEALT, MAX_DAMAGE_DEALT);
            windowPlayer.editorWeapons[weaponPointer].damage = EditorGUI.IntField(new Rect(fieldX, yOffset, fieldWidth, guiHeight), windowPlayer.editorWeapons[weaponPointer].damage);
            if (windowPlayer.editorWeapons[weaponPointer].damage < MIN_DAMAGE_DEALT)
                windowPlayer.editorWeapons[weaponPointer].damage = (int)MIN_DAMAGE_DEALT;
            if (windowPlayer.editorWeapons[weaponPointer].damage > MAX_DAMAGE_DEALT)
                windowPlayer.editorWeapons[weaponPointer].damage = (int)MAX_DAMAGE_DEALT;
            yOffset += yAddOffset;

            GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Max Clip Size:");
            windowPlayer.editorWeapons[weaponPointer].maxClipSize = (int)GUI.HorizontalSlider(new Rect(guiX, yOffset, guiX/2, guiHeight), windowPlayer.editorWeapons[weaponPointer].maxClipSize, MIN_CLIP_SIZE, MAX_CLIP_SIZE);
            windowPlayer.editorWeapons[weaponPointer].maxClipSize = EditorGUI.IntField(new Rect(fieldX, yOffset, fieldWidth, guiHeight), windowPlayer.editorWeapons[weaponPointer].maxClipSize);
            if(windowPlayer.editorWeapons[weaponPointer].maxClipSize < MIN_CLIP_SIZE)
                windowPlayer.editorWeapons[weaponPointer].maxClipSize = (int)MIN_CLIP_SIZE;
            if(windowPlayer.editorWeapons[weaponPointer].maxClipSize > MAX_CLIP_SIZE)
                windowPlayer.editorWeapons[weaponPointer].maxClipSize = (int)MAX_CLIP_SIZE;
            yOffset += yAddOffset;

            GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Cooldown:");
            windowPlayer.editorWeapons[weaponPointer].cooldown = GUI.HorizontalSlider(new Rect(guiX, yOffset, guiX/2, guiHeight), windowPlayer.editorWeapons[weaponPointer].cooldown, MIN_COOLDOWN_RELOAD, MAX_COOLDOWN_RELOAD);
            windowPlayer.editorWeapons[weaponPointer].cooldown = EditorGUI.FloatField(new Rect(fieldX, yOffset, fieldWidth, guiHeight), windowPlayer.editorWeapons[weaponPointer].cooldown);
            if(windowPlayer.editorWeapons[weaponPointer].cooldown < MIN_COOLDOWN_RELOAD)
                windowPlayer.editorWeapons[weaponPointer].cooldown = MIN_COOLDOWN_RELOAD;
            if(windowPlayer.editorWeapons[weaponPointer].cooldown > MAX_COOLDOWN_RELOAD)
                windowPlayer.editorWeapons[weaponPointer].cooldown = MAX_COOLDOWN_RELOAD;
            yOffset += yAddOffset;

            GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Reload Time:");
            windowPlayer.editorWeapons[weaponPointer].reloadTime = GUI.HorizontalSlider(new Rect(guiX, yOffset, guiX/2, guiHeight), windowPlayer.editorWeapons[weaponPointer].reloadTime, MIN_COOLDOWN_RELOAD, MAX_COOLDOWN_RELOAD);
            windowPlayer.editorWeapons[weaponPointer].reloadTime = EditorGUI.FloatField(new Rect(fieldX, yOffset, fieldWidth, guiHeight), windowPlayer.editorWeapons[weaponPointer].reloadTime);
            if(windowPlayer.editorWeapons[weaponPointer].reloadTime < MIN_COOLDOWN_RELOAD)
                windowPlayer.editorWeapons[weaponPointer].reloadTime = MIN_COOLDOWN_RELOAD;
            if(windowPlayer.editorWeapons[weaponPointer].reloadTime > MAX_COOLDOWN_RELOAD)
                windowPlayer.editorWeapons[weaponPointer].reloadTime = MAX_COOLDOWN_RELOAD;
            yOffset += yAddOffset;

            GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Damage Type:");
            windowPlayer.editorWeapons[weaponPointer].damageType = (DamageType)EditorGUI.EnumPopup(new Rect(guiX, yOffset, guiX, guiHeight), windowPlayer.editorWeapons[weaponPointer].damageType);
        GUI.EndGroup();
        //GUILayout.BeginArea(weaponsBoxRect);
        //    GUILayout.Label("Current weapon: " + windowPlayer.editorWeapons[weaponPointer].name);
        //    GUILayout.BeginHorizontal();
        //        EditorGUILayout.PrefixLabel("Weapon name: ");
        //        windowPlayer.editorWeapons[weaponPointer].name = GUILayout.TextField(windowPlayer.editorWeapons[weaponPointer].name);
        //    GUILayout.EndHorizontal();
        //    GUILayout.BeginHorizontal();

        //    GUILayout.EndHorizontal();
        //GUILayout.EndArea();
        //GUI.Box(weaponsBoxRect, GUIContent.none);
        //GUILayout.Box(GUIContent.none);
    }

    string[] IncreaseNamesArraySize(int newSize)
    {
        string[] newNamesArray = new string[newSize];
        for (int i = 0; i < weaponNames.Length; i++)
            newNamesArray[i] = weaponNames[i];
        newNamesArray[newSize - 1] = "";

        return newNamesArray;
    }

    string[] DecreaseNamesArraySize(int newSize, int removeNameIndex)
    {
        string[] newNamesArray = new string[newSize];
        for (int i = 0, j = 0; i < newSize; i++, j++)
        {
            if (j == removeNameIndex)
                j++;
            newNamesArray[i] = weaponNames[j];            
        }
              

        return newNamesArray;
    }
}
