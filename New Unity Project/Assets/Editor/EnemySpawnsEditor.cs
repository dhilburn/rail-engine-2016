﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(EnemySpawnContainer))]
public class EnemySpawnsEditor : Editor
{
    EnemySpawnContainer enemySpawns;

	void Awake () 
    {
        enemySpawns = (EnemySpawnContainer)target;
	}

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        if (GUILayout.Button("Edit enemy spawners"))
        {
            EnemySpawnsWindow.OpenEnemySpawnsWindow();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
