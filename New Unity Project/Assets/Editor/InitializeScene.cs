﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

/**
    @author Darrick Hilburn

    InitializeScene sets up a scene for the designer to work with
*/
public class InitializeScene : Editor
{
    private static PlayerRailEngine engine;
    private static GameObject initialWaypoint;

    /**
        @author Darrick Hilburn

        SetupScene creates a menu item for initializing the scene
        SetupScene calls functions to create the player, effects canvas, waypoint container,
        enemy spawns container, a default enemy spawn, and a default path
    */
    [MenuItem("Rail Engine/Initialize Scene")]
    public static void SetupScene()
    {        
        CreateEngineObject();        
        InitializeEngine();
        CreateEnemySpawnContainer();
    }

    /**
        @author Darrick Hilburn

        CreateEnemySpawnContainer creates the spawn container game object,
        which holds all enemy spawns in the scene.
        A default enemy spawner is also created.
    */
    private static void CreateEnemySpawnContainer()
    {
        GameObject spawnContainer = CreateEmptyObject("Enemy Spawners", WaypointEnumValues.ENEMY_SPAWN_CONTAINER);
        spawnContainer.AddComponent<EnemySpawnContainer>();
        spawnContainer.GetComponent<EnemySpawnContainer>().InitializeEnemySpawns();
        CreateEnemySpawn();
    }

    /**
        @author Darrick Hilburn

        CreateEnemySpawn creates an enemy spawner with a default enemy.
        The spawner is pulled from the prefabs, but does NOT have a prefab connection.
    */
    private static void CreateEnemySpawn()
    {
        GameObject spawn = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Dynamic Assets/Resources/Prefabs/Enemy Spawner.prefab", typeof(GameObject));
        
        Instantiate(spawn);

        GameObject inSceneSpawn = GameObject.FindWithTag(WaypointEnumValues.ENEMY_SPAWN_TAG);
        inSceneSpawn.name = "Spawn 1";
        inSceneSpawn.transform.position = engine.waypoints[(engine.waypoints.Count / 2) + 1].transform.position + (Vector3.left * 3);
        inSceneSpawn.transform.parent = GameObject.FindGameObjectWithTag(WaypointEnumValues.ENEMY_SPAWN_CONTAINER).transform;
        GameObject.FindGameObjectWithTag(WaypointEnumValues.ENEMY_SPAWN_CONTAINER).GetComponent<EnemySpawnContainer>().AddSpawner(inSceneSpawn);
    }

    /**
        @author Darrick Hilburn

        CreateEngineObject creates the player rail engine object.
        This object is a container that holds waypoints.
    */
    private static void CreateEngineObject()
    {
        GameObject playerEngine = CreateEmptyObject("Player Rail Engine", "Engine");
        playerEngine.AddComponent<PlayerRailEngine>();
        engine = playerEngine.GetComponent<PlayerRailEngine>();
    }    

    /**
        @author Darrick Hilburn

        CreatePath creates a hard-coded default path so that there is a starting
        point in the scene for both the engine tool and the designer.
    */
    private static void CreatePath()
    {
        GameObject[] waypoints = new GameObject[5];
        for (int i = 0; i < 4; i++)
        {
            waypoints[i] = CreateEmptyObject("Waypoint " + (i + 1), WaypointEnumValues.WAYPOINT_TAG);
            waypoints[i].AddComponent<WaypointScript>();
            waypoints[i].GetComponent<WaypointScript>().InitializeWaypoint(WaypointType.MOVEMENT, MovementType.STRAIGHT_LINE, FacingType.FREE_LOOK);
        }
        waypoints[4] = CreateEmptyObject("Waypoint 5", WaypointEnumValues.WAYPOINT_TAG);
        waypoints[4].AddComponent<WaypointScript>();
        waypoints[4].GetComponent<WaypointScript>().InitializeWaypoint();

        for (int i = 0; i < 4; i++)
        {
            waypoints[i].GetComponent<WaypointScript>().moveData.startPos = waypoints[i];
            waypoints[i].GetComponent<WaypointScript>().moveData.endPos = waypoints[i + 1];
        }
        waypoints[4].GetComponent<WaypointScript>().moveData.startPos = waypoints[4];
        waypoints[4].GetComponent<WaypointScript>().moveData.endPos = waypoints[4];

        waypoints[0].AddComponent<WaypointGizmo>();
        waypoints[0].transform.parent = GameObject.FindGameObjectWithTag("Engine").transform;
        waypoints[0].transform.position = Vector3.zero;
        engine.waypoints.Add(waypoints[0]);

        for (int i = 1; i <= 4; i++)
        {
            waypoints[i].AddComponent<WaypointGizmo>();
            waypoints[i].transform.parent = GameObject.FindGameObjectWithTag("Engine").transform;
            engine.waypoints.Add(waypoints[i]);
            waypoints[i].transform.position = waypoints[i - 1].transform.position + (Vector3.forward * 2);
        }

        initialWaypoint = waypoints[0];
    }

    /**
        @author Darrick Hilburn

        CreatePlayer creates the player object in the scene.
        The player object does have a prefab connection.
    */
    private static void CreatePlayer()
    {
        GameObject player = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Dynamic Assets/Resources/Prefabs/Player.prefab", typeof(GameObject));
        PrefabUtility.InstantiatePrefab(player);
        GameObject inScenePlayer = GameObject.FindGameObjectWithTag(WaypointEnumValues.PLAYER_TAG);
        inScenePlayer.name = "Player";
        inScenePlayer.transform.position = initialWaypoint.transform.position;
    }
    
    /**
        @author Darrick Hilburn

        CreateEffectsCanvas creates the effects canvas in the scene.
        The effects canvas does have a prefab connection.
    */
    private static void CreateEffectsCanvas()
    {
        GameObject effectsCanvas = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Dynamic Assets/Resources/Prefabs/Effects Canvas.prefab", typeof(GameObject));
        PrefabUtility.InstantiatePrefab(effectsCanvas);
        GameObject inSceneEffectsCanvas = GameObject.FindGameObjectWithTag("Effects Canvas");
        inSceneEffectsCanvas.name = "Effects Canvas";
    }

    /**
        @author Darrick Hilburn

        InitializeEngine sets up the engine with default values.
        This is done so that the tools have a starting point for the designer.
    */
    private static void InitializeEngine()
    {        
        engine.waypoints = new List<GameObject>(); 
        CreatePath();
        CreateEffectsCanvas();
        CreatePlayer();
        engine.player = GameObject.FindWithTag(WaypointEnumValues.PLAYER_TAG);
        engine.playerCam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();       
        engine.player.GetComponent<PlayerScript>().InitializeHealthGUI();
        engine.player.GetComponent<PlayerScript>().InitializeWeaponsGUI();
        engine.player.GetComponent<PlayerScript>().InitializeEditorWeaponsList();
        engine.SetMouseScript(GameObject.FindWithTag("MainCamera"));
        engine.SetEffectsCanvas();
    }

    /**
        @author Darrick Hilburn

        CreateEmptyObject creates an empty game object with name and tag at worldspace (0,0,0)
    */
    private static GameObject CreateEmptyObject(string name, string tag)
    {
        GameObject empty = new GameObject();
        empty.name = name;
        empty.tag = tag;
        empty.transform.position = Vector3.zero;
        return empty;
    }
}
