﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


public class EnemySpawnsWindow : EditorWindow
{
    public List<GameObject> enemySpawnObjects;      // List of in-scene enemy spawns

    EnemySpawnContainer spawnContainer;

    public string[] enemyNames;
    public string[] enemyWaypoints;
    public string[] enemyFocusNames;

    int enemyIndex = 0;
    int waypointIndex = 0;
    int focusIndex = 0;

    bool debugMode = false;

    static EnemySpawnsWindow window;

    GUIStyle titleText = new GUIStyle();
    GUIStyle boxTitle = new GUIStyle();
    
        

    public static void OpenEnemySpawnsWindow()
    {
        window = (EnemySpawnsWindow)EditorWindow.GetWindow(typeof(EnemySpawnsWindow));
        window.titleContent.text = "Enemy Spawners";
        window.minSize = new Vector2(550, 1000);
        window.maxSize = window.minSize;
        window.Show();
    }

    void OnFocus()
    {
        

        titleText.fontSize = 24;
        titleText.fontStyle = FontStyle.Bold;

        boxTitle.fontSize = 16;
        boxTitle.fontStyle = FontStyle.Bold;

        spawnContainer = GameObject.FindGameObjectWithTag(WaypointEnumValues.ENEMY_SPAWN_CONTAINER).GetComponent<EnemySpawnContainer>();
        enemySpawnObjects = spawnContainer.enemySpawnPositions;

        // Reset enemy index if it's outside the enemy spawns list
        if (enemyIndex > enemySpawnObjects.Count - 1)
            enemyIndex = 0;

        // Create a new enemy spawns list if there isn't one
        if (enemySpawnObjects == null)
            enemySpawnObjects = new List<GameObject>();
        // Create a new enemy spawner if the spawns list is empty
        if (enemySpawnObjects.Count < 1)
        {
            enemySpawnObjects.Add(CreateEnemySpawner(enemyIndex));
            enemyNames = new string[1];
            enemyNames[0] = "Enemy 1";
            enemySpawnObjects[0].name = enemyNames[0];
        }

        enemyNames = RenameEnemies();
        

        // Get waypoint information if this enemy is supposed to move
        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyBehaviour == EnemyAI.MOVE)
        {
            if (waypointIndex > enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1)
                waypointIndex = 0;
            if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints == null)
            {
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints = new List<GameObject>();
                CreateEnemySpawner(0);
            }

            enemyWaypoints = RenameWaypoints();
        }

    }

    void OnDestroy()
    {
        PushData();
    }

    void OnLostFocus()
    {
        debugMode = false;
        PushData();
    }

    void PushData()
    {
        // Push spawn point information
        spawnContainer.enemySpawnPositions = enemySpawnObjects;
    }

    void OnGUI()
    {
        Selection.activeGameObject = enemySpawnObjects[enemyIndex];
        HeaderGUI();
        GUI.Box(new Rect(5, 520, 540, 340), GUIContent.none);
        ButtonsGUI();
        EnemyGUI();
        AIGUI();        
        WaypointsGUI();
    }

    void HeaderGUI()
    {
        debugMode = GUI.Toggle(new Rect(462, 2.5f, 92, 16), debugMode, "Debug Mode ");

        GUI.Label(new Rect(150, 10, 400, 30), "Enemy Spawns Editor", titleText);

        titleText.fontSize = 18;
        titleText.fontStyle = FontStyle.Normal;

        GUI.Label(new Rect(110, 42, 150, 20), "Jump to Spawn: ", titleText);
        enemyIndex = EditorGUI.Popup(new Rect(300, 46, 100, 20), enemyIndex, enemyNames);

        GUI.Label(new Rect(109, 72, 150, 20), "Enemy position: ", titleText);
        enemySpawnObjects[enemyIndex].transform.position = EditorGUI.Vector3Field(new Rect(300, 76, 200, 20), GUIContent.none, enemySpawnObjects[enemyIndex].transform.position);

        #region Debug Code
        if (debugMode)
        {
            GUI.BeginGroup(new Rect(0, 580, 150, 60));
            if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveType != MovementType.WAIT)
            {
                GUI.Label(new Rect(0, 0, 150, 20), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].name);
                GUI.Label(new Rect(0, 20, 150, 20), "start: " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].transform.position);
                GUI.Label(new Rect(0, 40, 150, 20), "goto: " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex + 1].transform.position);
            }
            GUI.EndGroup();

            GUI.BeginGroup(new Rect(360, 580, 150, 60));
                GUI.Label(new Rect(0, 0, 150, 20), "Number of waypoints: " + enemySpawnObjects.Count);
                GUI.Label(new Rect(0, 20, 150, 20), "waypointIndex: " + waypointIndex);
            GUI.EndGroup();
        }
        #endregion
    }

    void ButtonsGUI()
    {
        Rect spawnsSelectiveButtonsSpace = new Rect(20, 100, 510, 40);
        Rect spawnsCreativeButtonSpace = new Rect(20, 880, 510, 40);

        Rect waypointSelectiveButtonsSpace = new Rect(20, 538, 510, 30);
        Rect waypointCreativeButtonSpace = new Rect(20, 810, 510, 30);

        #region Change Index Buttons

        #region Change Enemy Index Buttons

        GUI.BeginGroup(spawnsSelectiveButtonsSpace);
            GUI.color = Color.yellow;
            if (enemyIndex > 0)
            {
                if (GUI.Button(new Rect(0, 0, 108, 40), "<<\nFirst Enemy"))
                    enemyIndex = 0;
                if (GUI.Button(new Rect(108, 0, 108, 40), "<\nPrevious Enemy"))
                    enemyIndex--;
            }
            if (enemyIndex < enemySpawnObjects.Count - 1)
            {
                if (GUI.Button(new Rect(294, 0, 108, 40), ">\nNext Enemy"))
                    enemyIndex++;
                if (GUI.Button(new Rect(402, 0, 108, 40), ">>\nLast Enemy"))
                    enemyIndex = enemySpawnObjects.Count - 1;
            }
        GUI.EndGroup();

        #endregion

        #region Change Waypoint Index Button

        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyBehaviour == EnemyAI.MOVE)
        {
            GUI.BeginGroup(waypointSelectiveButtonsSpace);
            GUI.color = new Color(1, 0.5f, 0, 1);
            if (waypointIndex > 0)
            {
                if (GUI.Button(new Rect(5, 0, 118, 30), "<<\nFirst Waypoint"))
                    waypointIndex = 0;
                if (GUI.Button(new Rect(123, 0, 118, 30), "<<\nPrevious Waypoint"))
                    waypointIndex--;
            }
            if (waypointIndex < enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1)
            {
                if (GUI.Button(new Rect(264, 0, 118, 30), ">\nNext Waypoint"))
                    waypointIndex++;
                if (GUI.Button(new Rect(382, 0, 118, 30), ">>\nLast Waypoint"))
                    waypointIndex = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1;
            }
            GUI.EndGroup();
        }
        #endregion

        #endregion

        #region Add/Remove Buttons

        #region Add/Remove Enemies Buttons

        GUI.BeginGroup(spawnsCreativeButtonSpace);

        #region Insert Enemy Before Button
        GUI.color = Color.green;
        if(GUI.Button(new Rect(0, 0, 120, 40), "<Insert\nEnemy Before"))
        {
            enemySpawnObjects.Insert(enemyIndex, CreateEnemySpawnerBeforeIndex(enemyIndex));
            enemyNames = RenameEnemies();
        }
        #endregion

        #region Delete Enemy Button
        GUI.color = Color.red;
        if (enemySpawnObjects.Count > 1)
        {
            if (GUI.Button(new Rect(190, 0, 120, 40), "Delete Enemy"))
            {
                if (enemyIndex == enemySpawnObjects.Count - 1)
                {
                    enemyIndex--;
                    DestroyImmediate(enemySpawnObjects[enemySpawnObjects.Count - 1], true);
                    enemySpawnObjects.RemoveAt(enemySpawnObjects.Count - 1);
                }
                else
                {
                    DestroyImmediate(enemySpawnObjects[enemyIndex], true);
                    enemySpawnObjects.RemoveAt(enemyIndex);
                }
                enemyNames = RenameEnemies();
            }
        }
        #endregion

        #region Insert Enemy After Button
        if (enemyIndex < enemySpawnObjects.Count - 1)
        {
            GUI.color = Color.green;
            if (GUI.Button(new Rect(385, 0, 120, 40), "Insert\nEnemy After >"))
            {
                enemySpawnObjects.Insert(enemyIndex + 1, CreateEnemySpawnerAfterIndex(enemyIndex));
                enemyNames = RenameEnemies();
            }
        }
        else
        {
            GUI.color = Color.cyan;
            if (GUI.Button(new Rect(385, 0, 120, 40), "Add Enemy"))
            {
                enemySpawnObjects.Add(CreateEnemySpawnerAfterIndex(enemyIndex));
                enemyNames = RenameEnemies();
            }
        }
        #endregion

        GUI.EndGroup();

        #endregion

        #region Add/Remove Waypoints Buttons

        if(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyBehaviour == EnemyAI.MOVE)
        {
            GUI.BeginGroup(waypointCreativeButtonSpace);

            #region Insert Waypoint Before Button
                GUI.color = new Color(0, 0.75f, 0, 1);
                if (GUI.Button(new Rect(5, 0, 120, 30), "< Insert\nWaypoint before"))
                {
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Insert(waypointIndex, CreateWaypointBeforeIndex(waypointIndex));
                    enemyWaypoints = RenameWaypoints();
                }
            #endregion

            #region Delete Waypoint Button
                GUI.color = new Color(0.75f, 0, 0.75f, 1);
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count > 1)
                {
                    if (GUI.Button(new Rect(186, 0, 120, 30), "Delete Waypoint"))
                    {
                        if (waypointIndex == enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1)
                        {
                            waypointIndex--;
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 2].GetComponent<WaypointScript>().moveData.endPos = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 2];
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 2].GetComponent<WaypointScript>().moveType = MovementType.WAIT;
                            DestroyImmediate(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1]);
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.RemoveAt(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1);
                        }
                        else
                        {
                            if (waypointIndex > 0)
                            {
                                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex - 1].GetComponent<WaypointScript>().moveData.endPos = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex + 1];
                            }
                            DestroyImmediate(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex]);
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.RemoveAt(waypointIndex);
                        }
                        enemyWaypoints = RenameWaypoints();
                    }
                    if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count == 1 && enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[0].GetComponent<WaypointScript>().moveType != MovementType.WAIT)
                    {
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[0].GetComponent<WaypointScript>().moveData.startPos = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[0];
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[0].GetComponent<WaypointScript>().moveData.endPos = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[0];
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[0].GetComponent<WaypointScript>().moveType = MovementType.WAIT;
                    }
                }
            #endregion

            #region Insert Waypoint After Button
            if (waypointIndex < enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1)
            {
                GUI.color = new Color(0, 0.75f, 0, 1);
                if (GUI.Button(new Rect(372, 0, 120, 30), "Insert >\nWaypoint before"))
                {
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Insert(waypointIndex + 1, CreateWaypointAfterIndex(waypointIndex));
                    enemyWaypoints = RenameWaypoints();
                }
            }
            else
            {
                GUI.color = new Color(0, 0.75f, 0, 1);
                if (GUI.Button(new Rect(372, 0, 120, 30), "Add Waypoint"))
                {
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Add(CreateWaypointAfterIndex(waypointIndex));
                    enemyWaypoints = RenameWaypoints();
                }
            }

            #endregion

            GUI.EndGroup();
        }

        #endregion

        #endregion

        GUI.color = Color.white;        
    }

    void EnemyGUI()
    {
        Rect enemyDisplay = new Rect(150, 150, 250, 250);
        GUI.Box(enemyDisplay, GUIContent.none);

        GUI.BeginGroup(enemyDisplay);

            float guiHeight = 16f;
            float labelX = 100f;
            float yAddOffset = 20f;
            float yOffset = 4;
            float floatFieldWidth = 30f; 

            float enemyGUIX = 108f;
            float fieldGUIX = 165f;

            float minRadius = 1;
            float maxRadius = 10;
            int minHealth = 0;
            int maxHealth = 250;
            float minForce = 100;
            float maxForce = 10000;
            float minTime = 0;
            float maxWaitTime = 10;

            

            
            GUI.Label(new Rect(75, yOffset, labelX, guiHeight), "Enemy Data", boxTitle);
            yOffset += yAddOffset + 6;

            GUI.Label(new Rect(5, yOffset, labelX, guiHeight), "Enemy: ");
            if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemy == null)
                GUI.color = Color.red;
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemy = (GameObject)EditorGUI.ObjectField(new Rect(enemyGUIX, yOffset, enemyGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemy, typeof(GameObject));
            yOffset += yAddOffset;
            GUI.color = Color.white;
            if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemy != null)
            {
                
                GUI.Label(new Rect(5, yOffset, labelX, guiHeight), "Name: ");
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyName = GUI.TextField(new Rect(enemyGUIX, yOffset, enemyGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyName);
                enemySpawnObjects[enemyIndex].name = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyName;
                yOffset += yAddOffset;

                GUI.Label(new Rect(5, yOffset, labelX, guiHeight), "Aggro Radius: ");
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().aggroRadius = GUI.HorizontalSlider(new Rect(enemyGUIX, yOffset, enemyGUIX / 2, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().aggroRadius, minRadius, maxRadius);
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().aggroRadius = EditorGUI.FloatField(new Rect(fieldGUIX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().aggroRadius);
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().aggroRadius < minRadius)
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().aggroRadius = minRadius;
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().aggroRadius > maxRadius)
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().aggroRadius = maxRadius;
                yOffset += yAddOffset;

                GUI.Label(new Rect(5, yOffset, labelX, guiHeight), "Health: ");
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyHealth = (int)GUI.HorizontalSlider(new Rect(enemyGUIX, yOffset, enemyGUIX / 2, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyHealth, minHealth, maxHealth);
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyHealth = EditorGUI.IntField(new Rect(fieldGUIX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyHealth);
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyHealth < minHealth)
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyHealth = minHealth;
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyHealth > maxHealth)
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyHealth = maxHealth;
                yOffset += yAddOffset;

                GUI.Label(new Rect(5, yOffset, labelX, guiHeight), "Projectile: ");
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyProjectile == null)
                    GUI.color = Color.red;
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyProjectile = (GameObject)EditorGUI.ObjectField(new Rect(enemyGUIX, yOffset, enemyGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyProjectile, typeof(GameObject));
                yOffset += yAddOffset;

                GUI.color = Color.white;
                GUI.Label(new Rect(5, yOffset, labelX, guiHeight), "Projectile force: ");
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyProjectileForce = GUI.HorizontalSlider(new Rect(enemyGUIX, yOffset, enemyGUIX / 2, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyProjectileForce, minForce, maxForce);
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyProjectileForce = EditorGUI.FloatField(new Rect(fieldGUIX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyProjectileForce);
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyProjectileForce < minForce)
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyProjectileForce = minForce;
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyProjectileForce > maxForce)
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyProjectileForce = maxForce;
                yOffset += yAddOffset;

                GUI.Label(new Rect(5, yOffset, labelX, guiHeight), "Damage dealt: ");
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyDamage = (int)GUI.HorizontalSlider(new Rect(enemyGUIX, yOffset, enemyGUIX / 2, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyDamage, minHealth, maxHealth);
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyDamage = EditorGUI.IntField(new Rect(fieldGUIX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyDamage);
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyDamage < minHealth)
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyDamage = minHealth;
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyDamage > maxHealth)
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyDamage = maxHealth;
                yOffset += yAddOffset;

                GUI.Label(new Rect(5, yOffset, labelX, guiHeight), "Initial shot time: ");
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().initialShotTime = GUI.HorizontalSlider(new Rect(enemyGUIX, yOffset, enemyGUIX / 2, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().initialShotTime, minTime, maxWaitTime);
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().initialShotTime = EditorGUI.FloatField(new Rect(fieldGUIX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().initialShotTime);
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().initialShotTime < minTime)
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().initialShotTime = minTime;
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().initialShotTime > maxWaitTime)
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().initialShotTime = maxWaitTime;
                yOffset += yAddOffset;

                GUI.Label(new Rect(5, yOffset, labelX, guiHeight), "Time between shots: ");
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().cooldownTime = GUI.HorizontalSlider(new Rect(enemyGUIX, yOffset, enemyGUIX / 2, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().cooldownTime, minTime, maxWaitTime);
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().cooldownTime = EditorGUI.FloatField(new Rect(fieldGUIX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().cooldownTime);
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().cooldownTime < minTime)
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().cooldownTime = minTime;
                if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().cooldownTime > maxWaitTime)
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().cooldownTime = maxWaitTime;
                yOffset += yAddOffset;

                GUI.Label(new Rect(5, yOffset, labelX, guiHeight), "Resistance: ");
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyResistance = (DamageType)EditorGUI.EnumPopup(new Rect(enemyGUIX, yOffset, enemyGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyResistance);
                yOffset += yAddOffset;

                GUI.Label(new Rect(5, yOffset, labelX, guiHeight), "Weakness: ");
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWeakness = (DamageType)EditorGUI.EnumPopup(new Rect(enemyGUIX, yOffset, enemyGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWeakness);
                yOffset += yAddOffset;
            }
            
        GUI.EndGroup();
    }

    void AIGUI()
    {
        //Rect enemyDisplay = new Rect(150, 150, 250, 250);
        Rect aiDisplay = new Rect(150, 410, 250, 90);
        GUI.Box(aiDisplay, GUIContent.none);

        GUI.BeginGroup(aiDisplay);

            float guiHeight = 16f;
            float labelX = 100f;
            float yAddOffset = 20f;
            float yOffset = 4;
            float aiGUIX = 108f;

            GUIStyle boxTitle = new GUIStyle();
            boxTitle.fontSize = 16;
            boxTitle.fontStyle = FontStyle.Bold;

            if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemy != null)
            {
                GUI.Label(new Rect(75, yOffset, labelX, guiHeight), "Enemy AI", boxTitle);
                yOffset += yAddOffset + 6;

                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyFacesPlayer = GUI.Toggle(new Rect(5, yOffset, labelX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyFacesPlayer, "Face Player?");
                yOffset += yAddOffset;

                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyFacesPlayer = GUI.Toggle(new Rect(5, yOffset, labelX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyFacesPlayer, "Attack player?");
                yOffset += yAddOffset;

                GUI.Label(new Rect(5, yOffset, labelX, guiHeight), "Enemy AI: ");
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyBehaviour = (EnemyAI)EditorGUI.EnumPopup(new Rect(aiGUIX, yOffset, aiGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyBehaviour);
            }

        GUI.EndGroup();
    }

    void WaypointsGUI()
    {
        Rect waypointsDisplay = new Rect(25, 580, 640, 335);
        GUIStyle italic = new GUIStyle();
        italic.fontStyle = FontStyle.Italic;
        float guiHeight = 16f;
        float labelX = 100f;
        float yAddOffset = 20f;
        float yOffset = 4;
        float floatFieldWidth = 30f;


        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemy != null)
        {
            GUI.BeginGroup(waypointsDisplay);
            Rect movementsBox = new Rect(0, 0, 240, 215);
            Rect facingsBox = new Rect(255, 0, 240, 215);

            float movementGuiX = 108f;
            float movementFieldX = 165f;
            float facingGUIX = 80f;
            float facingFieldX = 165f;


            float minTime = 0;
            float maxDelay = 10;
            float maxMoveTime = 60;

            int minFocusPoints = 1;
            int maxFocusPoints = 10;

            GUI.Box(movementsBox, GUIContent.none);
            GUI.Box(facingsBox, GUIContent.none);

            GUI.BeginGroup(movementsBox);
            GUI.Label(new Rect(50, yOffset, labelX, guiHeight), "Movement Data", boxTitle);
            yOffset += yAddOffset;
            if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyBehaviour == EnemyAI.MOVE)
            {
                if (waypointIndex < enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1)
                {
                    GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Movement Type: ");
                    enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveType = (MovementType)EditorGUI.EnumPopup(new Rect(movementGuiX, yOffset, movementGuiX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveType);
                    yOffset += yAddOffset;
                }

                switch (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveType)
                {
                    case (MovementType.STRAIGHT_LINE):
                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Move Delay: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = GUI.HorizontalSlider(new Rect(movementGuiX, yOffset, movementGuiX / 2, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay, minTime, maxDelay);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = EditorGUI.FloatField(new Rect(movementFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay > maxDelay)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = maxDelay;
                        GUI.Label(new Rect(movementFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Move Time: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = GUI.HorizontalSlider(new Rect(movementGuiX, yOffset, movementGuiX / 2, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime, minTime, maxMoveTime);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = EditorGUI.FloatField(new Rect(movementFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime > maxMoveTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = maxMoveTime;
                        GUI.Label(new Rect(movementFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint != null)
                        {
                            DestroyImmediate(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint);
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint = null;
                        }

                        break;
                    case (MovementType.BEZIER_CURVE):
                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Move Delay: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = GUI.HorizontalSlider(new Rect(movementGuiX, yOffset, movementGuiX / 2, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay, minTime, maxDelay);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = EditorGUI.FloatField(new Rect(movementFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay > maxDelay)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay = maxDelay;
                        GUI.Label(new Rect(movementFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Move Time: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = GUI.HorizontalSlider(new Rect(movementGuiX, yOffset, movementGuiX / 2, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime, minTime, maxMoveTime);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = EditorGUI.FloatField(new Rect(movementFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime > maxMoveTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = maxMoveTime;
                        GUI.Label(new Rect(movementFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Curve point pos.");
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint == null)
                        {
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint = CreateCurvePoint();
                        }
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint.transform.position = EditorGUI.Vector3Field(new Rect(movementGuiX, yOffset, movementGuiX + 15, guiHeight), GUIContent.none, enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint.transform.position);

                        break;
                    default:
                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Wait Time: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = GUI.HorizontalSlider(new Rect(movementGuiX, yOffset, movementGuiX / 2, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime, minTime, maxMoveTime);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = EditorGUI.FloatField(new Rect(movementFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime > maxMoveTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime = maxMoveTime;
                        GUI.Label(new Rect(movementFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");

                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint != null)
                        {
                            DestroyImmediate(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint);
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint = null;
                        }
                        break;
                }
            }
            else
            {
                GUI.Label(new Rect(20, yOffset, labelX, guiHeight * 2), "No movement data necessary\nfor a non-moving enemy", italic);
            }
            GUI.EndGroup();

            yOffset = 4;

            GUI.BeginGroup(facingsBox);

            GUI.Label(new Rect(65, yOffset, labelX, guiHeight), "Facing Data", boxTitle);
            yOffset += yAddOffset;
            if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyBehaviour == EnemyAI.MOVE)
            {
                GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Facing Type: ");
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().facing = (FacingType)EditorGUI.EnumPopup(new Rect(facingGUIX, yOffset, 135, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().facing);
                yOffset += yAddOffset;
                switch (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().facing)
                {
                    case (FacingType.LOOK_AND_RETURN):
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = 1;
                        ChangeFocusPointsLists(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints);

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Face Delay: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay, minTime, maxDelay);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay > maxDelay)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Pan Time: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0], minTime, maxDelay);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0]);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] > maxDelay)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Focus Time: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0] = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0], minTime, maxDelay);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0] = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0]);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0] < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0] = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0] > maxDelay)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[0] = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Return Time: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime, minTime, maxDelay);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime > maxDelay)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Focus point pos.");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[0].transform.position = EditorGUI.Vector3Field(new Rect(labelX, yOffset, facingGUIX + facingGUIX / 2, guiHeight), GUIContent.none, enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[0].transform.position);

                        break;
                    case (FacingType.FORCED_DIRECTION):
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = 1;
                        ChangeFocusPointsLists(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints);

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Face Delay: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay, minTime, maxDelay);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay > maxDelay)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Pan Time: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0], minTime, maxDelay);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0]);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] > maxDelay)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[0] = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Focus point pos.");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[0].transform.position = EditorGUI.Vector3Field(new Rect(labelX, yOffset, facingGUIX + facingGUIX / 2, guiHeight), GUIContent.none, enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[0].transform.position);

                        break;
                    case (FacingType.LOOK_CHAIN):

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Face Delay: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay, minTime, maxDelay);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay > maxDelay)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceDelay = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Focus Points: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = (int)GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints, minFocusPoints, maxFocusPoints);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = EditorGUI.IntField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints < minFocusPoints)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = minFocusPoints;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints > maxFocusPoints)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = maxFocusPoints;

                        ChangeFocusPointsLists(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints);
                        if (focusIndex > 0 && focusIndex > enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints - 1)
                            focusIndex--;

                        enemyFocusNames = new string[enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints];
                        for (int i = 0; i < enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints; i++)
                            enemyFocusNames[i] = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[i].name;
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(80, yOffset, labelX, guiHeight), "Focus Point " + (focusIndex + 1));
                        yOffset += yAddOffset;
                        GUI.BeginGroup(new Rect(0, yOffset, 240, guiHeight));
                        if (focusIndex > 0)
                        {
                            GUI.color = Color.white;
                            if (GUI.Button(new Rect(0, 0, 40, guiHeight), "<<"))
                                focusIndex = 0;
                            if (GUI.Button(new Rect(40, 0, 40, guiHeight), "<"))
                                focusIndex--;
                        }
                        focusIndex = EditorGUI.Popup(new Rect(80, 0, 80, guiHeight), focusIndex, enemyFocusNames);
                        if (focusIndex < enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints)
                        {
                            if (GUI.Button(new Rect(160, 0, 40, guiHeight), ">"))
                                focusIndex++;
                            if (GUI.Button(new Rect(200, 0, 40, guiHeight), ">>"))
                                focusIndex = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints - 1;
                        }
                        GUI.EndGroup();
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Pan Time: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusIndex] = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusIndex], minTime, maxDelay);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusIndex] = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusIndex]);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusIndex] < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusIndex] = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusIndex] > maxDelay)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes[focusIndex] = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Focus Time: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusIndex] = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusIndex], minTime, maxDelay);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusIndex] = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusIndex]);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusIndex] < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusIndex] = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusIndex] > maxDelay)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes[focusIndex] = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Focus point pos.");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[focusIndex].transform.position = EditorGUI.Vector3Field(new Rect(labelX, yOffset, facingGUIX + facingGUIX / 2, guiHeight), GUIContent.none, enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[focusIndex].transform.position);
                        yOffset += yAddOffset;

                        GUI.Label(new Rect(0, yOffset, labelX, guiHeight), "Return Time: ");
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = GUI.HorizontalSlider(new Rect(facingGUIX, yOffset, facingGUIX, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime, minTime, maxDelay);
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = EditorGUI.FloatField(new Rect(facingFieldX, yOffset, floatFieldWidth, guiHeight), enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime);
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime < minTime)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = minTime;
                        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime > maxDelay)
                            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.returnTime = maxDelay;
                        GUI.Label(new Rect(facingFieldX + floatFieldWidth, yOffset, 20, guiHeight), "s");
                        yOffset += yAddOffset;

                        break;

                    default:
                        enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints = 0;
                        ChangeFocusPointsLists(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints);
                        GUI.Label(new Rect(20, yOffset, labelX * 2, guiHeight * 2), "This enemy has no facing.\nNo parameters needed.", italic);
                        break;
                }
            }
            else
            {
                GUI.Label(new Rect(35, yOffset, labelX, guiHeight * 2), "No facing data necessary\nfor a non-moving enemy", italic);
            }
            GUI.EndGroup();


            GUI.EndGroup();
        }
    }

    GameObject CreateEnemySpawnerBeforeIndex(int insertIndex)
    {
        GameObject spawner = CreateEnemySpawner(insertIndex + 1);

        #region New Spawner Transform Assign

        if (insertIndex > 0)
        {
            spawner.transform.position = WaypointsMidway(enemySpawnObjects[insertIndex - 1].transform.position, enemySpawnObjects[insertIndex].transform.position);
        }
        else
        {
            spawner.transform.position = enemySpawnObjects[0].transform.position + (Vector3.back * 2);
        }

        #endregion

        return spawner;
    }

    GameObject CreateEnemySpawnerAfterIndex(int insertIndex)
    {
        GameObject spawner = CreateEnemySpawner(insertIndex + 2);
        if (insertIndex < enemySpawnObjects.Count - 1)
        {
            spawner.transform.position = WaypointsMidway(enemySpawnObjects[insertIndex].transform.position, enemySpawnObjects[insertIndex + 1].transform.position);
        }
        else
        {
            spawner.transform.position = enemySpawnObjects[enemySpawnObjects.Count - 1].transform.position + (Vector3.forward * 2);
        }

        return spawner;
    }

    GameObject CreateEnemySpawner(int index)
    {
        //GameObject newSpawn = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Dynamic Assets/Resources/Prefabs/Enemy Spawner.prefab", typeof(GameObject));
        GameObject newSpawn = new GameObject();
        newSpawn.name = "Spawn " + (index);
        newSpawn.tag = WaypointEnumValues.ENEMY_SPAWN_TAG;
        newSpawn.transform.parent = GameObject.FindGameObjectWithTag(WaypointEnumValues.ENEMY_SPAWN_CONTAINER).transform;
        newSpawn.AddComponent<EnemySpawner>();
        newSpawn.AddComponent<WaypointScript>();
        newSpawn.AddComponent<WaypointGizmo>();
        newSpawn.AddComponent<EnemySpawnGizmo>();
        newSpawn.GetComponent<WaypointScript>().InitializeWaypoint();
        newSpawn.GetComponent<EnemySpawner>().enemyWaypoints = new List<GameObject>();
        newSpawn.GetComponent<EnemySpawner>().enemyWaypoints.Add(newSpawn);
        newSpawn.GetComponent<EnemySpawner>().enemyWaypoints[0].GetComponent<WaypointScript>().moveData.startPos = newSpawn.GetComponent<EnemySpawner>().enemyWaypoints[0];
        newSpawn.GetComponent<EnemySpawner>().enemyWaypoints[0].GetComponent<WaypointScript>().moveData.endPos = newSpawn.GetComponent<EnemySpawner>().enemyWaypoints[0];
        newSpawn.GetComponent<EnemySpawner>().enemyWaypoints[0].GetComponent<WaypointScript>().moveType = MovementType.WAIT;
        newSpawn.GetComponent<EnemySpawner>().numWaypoints = 1;

        return newSpawn;
    }

    GameObject CreateWaypointBeforeIndex(int insertIndex)
    {
        GameObject waypoint = CreateWaypoint((insertIndex + 1));
        Debug.Log("Creating new " + waypoint.name);

        #region New Waypoint Transform assign
        // Assign the new waypoint transform
        // If inserting after last waypoint, place 2 units ahead of last waypoint
        // Otherwise, place between the current and next waypoints
        if (insertIndex > 0)
        {
            Debug.Log("Point 1: " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex - 1].transform.position);
            Debug.Log("Point 2: " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].transform.position);
            Debug.Log("Placing new point between " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex - 1].transform.position + " and " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].transform.position + "\n---> " + WaypointsMidway(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex - 1].transform.position, enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].transform.position));
            waypoint.transform.position = WaypointsMidway(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex - 1].transform.position, enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].transform.position);
        }
        else
        {
            Debug.Log("Placing new point at start ---> " + (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[0].transform.position + (Vector3.back * 2)));
            waypoint.transform.position = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[0].transform.position + (Vector3.back * 2);
        }
        #endregion

        #region Add New Waypoint to Rail Engine

        // Link the waypoint into the engine waypoints list

        // If inserting in the middle of the list,
        //    The previous point movement becomes a straight line if it's a wait with the waypoint as its endpoint,
        //    and the waypoint is also a straight line movement with the next waypoint as its endpoint

        // If inserting at the end of the list,
        //    The previous point movement becomes a straight line if it's a wait with the waypoint as its endpoint,
        //    and the waypoint is a wait movement.
        if (insertIndex > 0)
        {
            if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex - 1].GetComponent<WaypointScript>().moveType == MovementType.WAIT)
            {
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex - 1].GetComponent<WaypointScript>().moveType = MovementType.STRAIGHT_LINE;
                Debug.Log("The waypoint at " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex - 1].transform.position + " now has a straight line movement");
            }
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex - 1].GetComponent<WaypointScript>().moveData.endPos = waypoint;
            Debug.Log("The waypoint at " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex - 1].transform.position + " now goes to new point @ " + waypoint.transform.position);

            waypoint.GetComponent<WaypointScript>().moveData.startPos = waypoint;
            waypoint.GetComponent<WaypointScript>().moveData.endPos = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex];
            waypoint.GetComponent<WaypointScript>().moveType = MovementType.STRAIGHT_LINE;
            Debug.Log("new point @ " + waypoint.transform.position + " goes to " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].transform.position);
        }
        else
        {
            waypoint.GetComponent<WaypointScript>().moveType = MovementType.STRAIGHT_LINE;
            waypoint.GetComponent<WaypointScript>().moveData.startPos = waypoint;
            waypoint.GetComponent<WaypointScript>().moveData.endPos = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[0];
            Debug.Log("new point @ " + waypoint.transform.position + " goes to " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[0].transform.position);
        }
        #endregion

        return waypoint;
    }

    GameObject CreateWaypointAfterIndex(int insertIndex)
    {
        GameObject waypoint = CreateWaypoint((insertIndex + 2));
        Debug.Log("Creating new " + waypoint.name);

        #region New Waypoint Transform assign
        // Assign the new waypoint transform
        // If inserting after last waypoint, place 2 units ahead of last waypoint
        // Otherwise, place between the current and next waypoints
        if (insertIndex < enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1)
        {
            Debug.Log("Point 1: " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].transform.position);
            Debug.Log("Point 2: " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex + 1].transform.position);
            Debug.Log("Placing new point between " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].transform.position + " and " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex + 1].transform.position + "\n---> " + WaypointsMidway(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].transform.position, enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex + 1].transform.position));
            waypoint.transform.position = WaypointsMidway(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].transform.position, enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex + 1].transform.position);
        }
        else
        {
            Debug.Log("Placing new point at end ---> " + (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1].transform.position + (Vector3.forward * 2)));
            waypoint.transform.position = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1].transform.position + (Vector3.forward * 2);
        }
        #endregion

        #region Add New Waypoint to Rail Engine

        // Link the waypoint into the engine waypoints list

        // If inserting in the middle of the list,
        //    The previous point movement becomes a straight line if it's a wait with the waypoint as its endpoint,
        //    and the waypoint is also a straight line movement with the next waypoint as its endpoint

        // If inserting at the end of the list,
        //    The previous point movement becomes a straight line if it's a wait with the waypoint as its endpoint,
        //    and the waypoint is a wait movement.

        if (insertIndex < enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1)
        {
            if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex + 1].GetComponent<WaypointScript>().moveType == MovementType.WAIT && enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.IndexOf(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex + 1]) != (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count - 1))
            {
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex + 1].GetComponent<WaypointScript>().moveType = MovementType.STRAIGHT_LINE;
                Debug.Log("The waypoint at " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].transform.position + " now has a straight line movement");
            }
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].GetComponent<WaypointScript>().moveData.endPos = waypoint;
            Debug.Log("The waypoint at " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].transform.position + " now goes to new point @ " + waypoint.transform.position);

            waypoint.GetComponent<WaypointScript>().moveData.startPos = waypoint;
            waypoint.GetComponent<WaypointScript>().moveData.endPos = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex + 1];
            waypoint.GetComponent<WaypointScript>().moveType = MovementType.STRAIGHT_LINE;
            Debug.Log("new point @ " + waypoint.transform.position + " goes to " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex + 1].transform.position);

        }
        else
        {
            if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].GetComponent<WaypointScript>().moveType == MovementType.WAIT)
            {
                enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].GetComponent<WaypointScript>().moveType = MovementType.STRAIGHT_LINE;
                Debug.Log("The waypoint at " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].transform.position + " now has a straight line movement");
            }
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex].GetComponent<WaypointScript>().moveData.endPos = waypoint;
            if (insertIndex - 1 > 0)
                Debug.Log("The waypoint at " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[insertIndex - 1].transform.position + " now goes to new point @ " + waypoint.transform.position);
            else
                Debug.Log("The waypoint at " + enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[0].transform.position + " now goes to new point @ " + waypoint.transform.position);

            waypoint.GetComponent<WaypointScript>().moveData.startPos = waypoint;
            waypoint.GetComponent<WaypointScript>().moveData.endPos = waypoint;
            Debug.Log("New point is end of path");
        }

        #endregion

        return waypoint;
    }

    GameObject CreateWaypoint(int index)
    {
        GameObject waypoint = new GameObject();
        waypoint.name = "Waypoint " + (index);
        waypoint.tag = WaypointEnumValues.ENEMY_WAYPOINT_TAG;
        waypoint.transform.parent = enemySpawnObjects[enemyIndex].gameObject.transform;
        waypoint.AddComponent<WaypointScript>();
        waypoint.GetComponent<WaypointScript>().InitializeWaypoint();
        waypoint.AddComponent<WaypointGizmo>();

        return waypoint;
    }

    Vector3 WaypointsMidway(Vector3 waypointBefore, Vector3 waypointAfter)
    {
        float midwayX = (waypointBefore.x + waypointAfter.x) / 2f;
        float midwayY = (waypointBefore.y + waypointAfter.y) / 2f;
        float midwayZ = (waypointBefore.z + waypointAfter.z) / 2f;

        return new Vector3(midwayX, midwayY, midwayZ);
    }

    string[] RenameEnemies()
    {
        string[] newNames = new string[enemySpawnObjects.Count];
        for (int i = 0; i < enemySpawnObjects.Count; i++)
        {
            Debug.Log("Renaming " + enemySpawnObjects[i].name);
            enemySpawnObjects[i].name = "Enemy " + (i + 1);
            newNames[i] = enemySpawnObjects[i].name;
            Debug.Log("New name: " + enemySpawnObjects[i].name);
        }
        return newNames;
    }

    string[] RenameWaypoints()
    {
        string[] newNamesList = new string[enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count];
        for (int i = 0; i < enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints.Count; i++)
        {
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[i].name = "Waypoint " + (i + 1);
            newNamesList[i] = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[i].name;
        }

        return newNamesList;
    }

    string[] RenameFocusPoints()
    {
        string[] newNames = new string[enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count];
        for (int i = 0; i < newNames.Length; i++)
        {
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[i].name = "Focus " + (i + 1);
            newNames[i] = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[i].name;
        }

        return newNames;
    }

    void ChangeFocusPointsLists(int numFocusPoints)
    {
        // Make sure all three lists exist
        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints == null)
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints = new List<GameObject>();
        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes == null)
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes = new List<float>();
        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes == null)
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes = new List<float>();

        if (numFocusPoints > enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count)
            AddFocusPoints(numFocusPoints);
        else if (numFocusPoints < enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count)
            DeleteFocusPoints(numFocusPoints);       
    }

    void AddFocusPoints(int numFocusPoints)
    {
        for (int i = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count; i < numFocusPoints; i++)
        {
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Add(CreateFocusPoint(("Focus " + (waypointIndex + 1) + "." + (i + 1)), i - 1));
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes.Add(0);
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes.Add(0);
        }        
    }

    void DeleteFocusPoints(int numFocusPoints)
    {

        for (int i = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count; i > numFocusPoints; i--)
        {
            DestroyImmediate(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count - 1]);
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.RemoveAt(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count - 1);
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.panTimes.RemoveAt(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count - 1);
            enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.faceTimes.RemoveAt(enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints.Count - 1);
        }
    }

    GameObject CreateCurvePoint()
    {
        GameObject curvePoint = new GameObject();
        curvePoint.name = "Curve " + (waypointIndex + 1);
        curvePoint.tag = WaypointEnumValues.ENEMY_WAYPOINT_TAG;
        curvePoint.transform.parent = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].transform;
        curvePoint.transform.localPosition = Vector3.forward + (Vector3.left) * 2;
        curvePoint.AddComponent<WaypointScript>();
        curvePoint.GetComponent<WaypointScript>().waypointType = WaypointType.CURVE;
        curvePoint.AddComponent<WaypointGizmo>();

        return curvePoint;
    }

    GameObject CreateFocusPoint(string name, int previousFocusPointIndex)
    {
        GameObject focusPoint = new GameObject();
        focusPoint.name = name;
        focusPoint.tag = WaypointEnumValues.ENEMY_WAYPOINT_TAG;
        focusPoint.transform.parent = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].transform;
        if (enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.numFocusPoints < 2)
            focusPoint.transform.localPosition = (Vector3.one * 2) + (Vector3.left * 4);
        else
            focusPoint.transform.localPosition = enemySpawnObjects[enemyIndex].GetComponent<EnemySpawner>().enemyWaypoints[waypointIndex].GetComponent<WaypointScript>().faceData.focusPoints[previousFocusPointIndex].transform.position + (Vector3.right * 2);
        focusPoint.AddComponent<WaypointScript>();
        focusPoint.GetComponent<WaypointScript>().waypointType = WaypointType.FOCUS;
        focusPoint.AddComponent<WaypointGizmo>();

        return focusPoint;
    }
}
