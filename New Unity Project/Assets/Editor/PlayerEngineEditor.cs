﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(PlayerRailEngine))]
public class PlayerEngineEditor : Editor {

    PlayerRailEngine engineScript;

    void Awake()
    {
        engineScript = (PlayerRailEngine)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        if (GUILayout.Button("Edit Waypoint Engine")) 
        { 
            PlayerEngineWindow.OpenRailEngineWindow();
        }
            
        serializedObject.ApplyModifiedProperties();
    }
}
