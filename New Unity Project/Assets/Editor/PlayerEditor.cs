﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(PlayerScript))]
public class PlayerEditor : Editor
{
    PlayerScript player;

    void Awake()
    {
        player = (PlayerScript)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();        

        SerializedProperty health = serializedObject.FindProperty("playerHealth");
        SerializedProperty weapons = serializedObject.FindProperty("editorWeapons");
        

        EditorGUILayout.PropertyField(health);

        if (GUILayout.Button("Debug serialized weapons array"))
        {
            Debug.Log(weapons.displayName);

        }

        if (GUILayout.Button("Edit Player Weapons"))
        {
            PlayerWindow.OpenPlayerEditorWindow();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
