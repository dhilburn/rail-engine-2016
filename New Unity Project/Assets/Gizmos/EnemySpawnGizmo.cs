﻿using UnityEngine;

/**
    @author Darrick Hilburn

    EnemySpawnGizmo shows the radii for when the enemy will spawn in and despawn.
*/
public class EnemySpawnGizmo : MonoBehaviour
{
    void OnDrawGizmosSelected()
    {
        // Get the spawn radius
        float aggroRadius = GetComponent<EnemySpawner>().aggroRadius;
        // Draw the spawn radius
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, aggroRadius);
        // Draw the despawn radius
        Gizmos.color = Color.grey;
        Gizmos.DrawWireSphere(transform.position, aggroRadius * 2);
    }
}
