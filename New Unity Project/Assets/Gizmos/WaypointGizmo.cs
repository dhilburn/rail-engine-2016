﻿using UnityEngine;

/**
    @author Darrick Hilburn

    WaypointGizmo draws the waypoint position for all waypoints
    and path for movement waypoints
*/
public class WaypointGizmo : MonoBehaviour
{
    [HideInInspector]
    public float drawRadius = 0.2f; // Radius of draw sphere for waypoint position
    [HideInInspector]
    public int bezierSteps = 10;    // Number of lines to draw for a bezier move gizmo

    void OnDrawGizmos()
    {
        if (gameObject.GetComponent<WaypointScript>().waypointType == WaypointType.MOVEMENT)
        {
            if (gameObject.GetComponent<WaypointScript>().moveData.startPos == null)
            {
                gameObject.GetComponent<WaypointScript>().moveData.startPos = gameObject;
            }
            if (gameObject.GetComponent<WaypointScript>().moveData.endPos == null)
            {
                gameObject.GetComponent<WaypointScript>().moveData.endPos = gameObject;
            }
        }
        DrawPoint();
        DrawPath();
    }

    /**
        @author Darrick Hilburn

        DrawPoint draws a sphere at the waypoint position.
        The color of the sphere depends on the waypoint type.
    */
    void DrawPoint()
    {
        WaypointType drawPoint = GetComponent<WaypointScript>().waypointType;
        if (gameObject.tag.Equals(WaypointEnumValues.WAYPOINT_TAG))
        {
            switch (drawPoint)
            {
                case (WaypointType.MOVEMENT):
                    Gizmos.color = Color.cyan;
                    break;
                case (WaypointType.FOCUS):
                    Gizmos.color = Color.magenta;
                    break;
                case (WaypointType.CURVE):
                    Gizmos.color = Color.yellow;
                    break;
                default:
                    Gizmos.color = Color.red;
                    break;
            }
        }
        else if (gameObject.tag.Equals(WaypointEnumValues.ENEMY_WAYPOINT_TAG))
        {
            switch (drawPoint)
            {
                case (WaypointType.MOVEMENT):
                    Color purple = new Color(1, 0.016f, 0.92f, 1);
                    Gizmos.color = purple;
                    break;
                case (WaypointType.FOCUS):
                    Gizmos.color = Color.green;
                    break;
                case (WaypointType.CURVE):
                    Gizmos.color = Color.grey;
                    break;
                default:
                    Gizmos.color = Color.red;
                    break;
            }
        }
        Gizmos.DrawSphere(transform.position, drawRadius);
    }

    /**
        @author Darrick Hilburn

        DrawPath draws the path between waypoints for a movement.
        The color of the path depends on the movement type.
    */
    void DrawPath()
    {
        if (GetComponent<WaypointScript>().waypointType.Equals(WaypointType.MOVEMENT))
        {
            MovementType drawMove = GetComponent<WaypointScript>().moveType;
            Vector3 startPos = GetComponent<WaypointScript>().moveData.startPos.transform.position;
            Vector3 endPos = ((GetComponent<WaypointScript>().moveData.endPos.transform.position != null) ? GetComponent<WaypointScript>().moveData.endPos.transform.position : startPos);
            Vector3 curvePos = Vector3.zero;
            if(GetComponent<WaypointScript>().moveType.Equals(MovementType.BEZIER_CURVE))
                curvePos = GetComponent<WaypointScript>().moveData.curvePoint.transform.position;

            if (gameObject.tag.Equals(WaypointEnumValues.WAYPOINT_TAG))
            {
                switch (drawMove)
                {
                    case (MovementType.STRAIGHT_LINE):
                        Gizmos.color = Color.blue;
                        Gizmos.DrawLine(startPos, endPos);
                        break;
                    case (MovementType.BEZIER_CURVE):
                        Gizmos.color = Color.green;
                        DrawBezier(startPos, endPos, curvePos);
                        break;
                    default:
                        Gizmos.color = Color.white;
                        break;
                }
            }
            else if (gameObject.tag.Equals(WaypointEnumValues.ENEMY_WAYPOINT_TAG) || gameObject.tag.Equals(WaypointEnumValues.ENEMY_SPAWN_TAG))
            {
                switch (drawMove)
                {
                    case (MovementType.STRAIGHT_LINE):
                        Gizmos.color = Color.red;
                        Gizmos.DrawLine(startPos, endPos);
                        break;
                    case (MovementType.BEZIER_CURVE):
                        Gizmos.color = Color.black;
                        DrawBezier(startPos, endPos, curvePos);
                        break;
                    default:
                        Gizmos.color = Color.white;
                        break;
                }
            }
        }
    }

    /**
        @author Darrick Hilburn

        DrawBezier draws the individual lines for a bezier movement.
    */
    void DrawBezier(Vector3 start, Vector3 end, Vector3 curve)
    {
        Vector3 lineStart = GetPoint(start, end, curve, 0);
        for (int i = 1; i <= bezierSteps; i++)
        {
            Vector3 lineEnd = GetPoint(start, end, curve, ((float)i / (float)bezierSteps));
            Gizmos.DrawLine(lineStart, lineEnd);
            lineStart = lineEnd;
        }
    }

    /**
        @author Linda Lane

        GetPoint gets the endpoint for a bezier curve line segment.
    */
    Vector3 GetPoint(Vector3 startPos, Vector3 endPos, Vector3 curvePos, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1.0f - t;
        return (oneMinusT * oneMinusT * startPos) + (2.0f * oneMinusT * t * curvePos) + (t * t * endPos);
    }
}
