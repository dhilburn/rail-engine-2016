﻿using UnityEngine;
/**
    @author Darrick Hilburn

    PlayerBulletSpawnGizmo shows where projectiles will spawn from the player.

    Since shooting has been set to cursor position instead of a position anchored to the player,
    this script is technically obsolete.
*/
public class PlayerBulletSpawnGizmo : MonoBehaviour 
{    
    [SerializeField]
    float cubeSize = 0.1f; // Draw cube size

    void OnDrawGizmosSelected()
    {
        // Draw a cube at the projectile spawn point
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position, Vector3.one * cubeSize);
    }
}
