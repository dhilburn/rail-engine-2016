﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy; //

    public string enemyName; //
    public float aggroRadius; //
    public int enemyHealth; //
    public GameObject enemyProjectile; // 
    public int enemyDamage; // 
    public float enemyProjectileForce; // 
    public float initialShotTime; //
    public float cooldownTime; //
    public DamageType enemyResistance; //
    public DamageType enemyWeakness; // 
    public bool enemyFacesPlayer; //
    public bool enemyAttacksPlayer; //
    public EnemyAI enemyBehaviour; //
    public List<GameObject> enemyWaypoints; //
    public int numWaypoints;

    float despawnRadius;

    bool enemySpawned = false;
    GameObject player;
    GameObject enemyInstance;

    public EnemySpawner()
    {
        InitializeEnemyStats();
    }

    public EnemySpawner(float passRadius, GameObject passEnemy, int health, GameObject projectile, int damage, float projForce, float passInitialShotTime, float cooldown, DamageType resistance, DamageType weakness, bool facePlayer, bool attackPlayer, EnemyAI behaviour)
    {
        InitializeEnemyStats(passRadius, passEnemy, health, projectile, damage, projForce, passInitialShotTime, cooldown, resistance, weakness, facePlayer, attackPlayer, behaviour);
    }

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindWithTag(WaypointEnumValues.PLAYER_TAG);
        InitializeEnemy();
        despawnRadius = aggroRadius * 2;
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(player.transform.position, transform.position);
        // Spawn the enemy when the player is in the aggro radius
        
        if (distance < aggroRadius && !enemySpawned)
        {
            enemyInstance = Instantiate(enemy);
            enemyInstance.name = "Enemy";
            enemyInstance.tag = WaypointEnumValues.ENEMY_TAG;
            enemyInstance.transform.parent = transform;
            enemyInstance.transform.position = transform.position;            
            enemySpawned = true;
            Debug.Log("Enemy spawned");
        }
        // Despawn the enemy when the player is outside the despawn radius
        if (distance > despawnRadius && enemySpawned)
        {
            Destroy(enemyInstance);
            enemySpawned = false;
            Debug.Log("Enemy despawned");
        }
    }

    public void InitializeEnemyStats()
    {
        enemy = null;
        enemyName = "";
        aggroRadius = 1.5f;
        enemyHealth = 100;
        enemyProjectile = null;
        enemyDamage = 10;
        enemyProjectileForce = 500;
        initialShotTime = 2.0f;
        cooldownTime = 1.5f;
        enemyResistance = DamageType.NONE;
        enemyWeakness = DamageType.NONE;
        enemyFacesPlayer = true;
        enemyAttacksPlayer = true;
        enemyBehaviour = EnemyAI.STAND;
    }

    public void InitializeEnemyStats(float passRadius, GameObject passEnemy, int health, GameObject projectile, int damage, float projForce, float passInitialShotTime, float cooldown, DamageType resistance, DamageType weakness, bool facePlayer, bool attackPlayer, EnemyAI behaviour)
    {
        if (passRadius < 2)
            aggroRadius = 2;
        else
            aggroRadius = passRadius;

        enemy = passEnemy;

        enemyHealth = health;

        enemyProjectile = projectile;

        if (damage < 0)
            enemyDamage = 0;
        else
            enemyDamage = damage;

        if (projForce < 500)
            projForce = 500;
        else if (projForce > 10000)
            projForce = 10000;
        enemyProjectileForce = projForce;

        if (passInitialShotTime < 0)
            passInitialShotTime = 0;
        else if (passInitialShotTime > 10)
            passInitialShotTime = 10;
        initialShotTime = passInitialShotTime;

        if (cooldown < 0)
            cooldown = 0;
        else if (cooldown > 10)
            cooldown = 10;
        cooldownTime = cooldown;

        enemyResistance = resistance;
        enemyWeakness = weakness;
        enemyFacesPlayer = facePlayer;
        enemyAttacksPlayer = attackPlayer;
        enemyBehaviour = behaviour;
    }

    void InitializeEnemy()
    {
        enemy.GetComponent<EnemyScript>().health = enemyHealth;
        enemy.GetComponent<EnemyScript>().projectile = enemyProjectile;
        enemy.GetComponent<EnemyScript>().firstShotTime = initialShotTime;
        enemy.GetComponent<EnemyScript>().repeatShotTime = cooldownTime;
        enemy.GetComponent<EnemyScript>().resistance = enemyResistance;
        enemy.GetComponent<EnemyScript>().weakness = enemyWeakness;
        enemy.GetComponent<EnemyScript>().facePlayer = enemyFacesPlayer;
        enemy.GetComponent<EnemyScript>().attacks = enemyAttacksPlayer;
        enemy.GetComponent<EnemyScript>().enemyAI = enemyBehaviour;
        enemy.GetComponent<EnemyScript>().enemyWaypoints = enemyWaypoints;
        enemy.GetComponent<EnemyScript>().projectile.GetComponent<BulletScript>().damage = enemyDamage;
        enemy.GetComponent<EnemyScript>().projectile.GetComponent<BulletScript>().force = enemyProjectileForce;
        enemy.GetComponent<EnemyScript>().projectile.GetComponent<BulletScript>().damageType = DamageType.NONE;
    }

    public void EnemyKilled()
    {
        enemySpawned = false;
    }

    public bool EnemyExists()
    {
        return enemySpawned;
    }
}
