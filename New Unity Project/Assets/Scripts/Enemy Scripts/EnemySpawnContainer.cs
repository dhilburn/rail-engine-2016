﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawnContainer : MonoBehaviour 
{
    public List<GameObject> enemySpawnPositions;

    public void InitializeEnemySpawns()
    {
        enemySpawnPositions = new List<GameObject>();
    }

    public void AddSpawner(GameObject spawner)
    {
        enemySpawnPositions.Add(spawner);
    }
}
