﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyScript : MonoBehaviour
{
    //[HideInInspector]
    public int health;
    //[HideInInspector]
    public GameObject projectile;
    //[HideInInspector]
    public float firstShotTime;
    //[HideInInspector]
    public float repeatShotTime;
    //[HideInInspector]
    public DamageType resistance;
    //[HideInInspector]
    public DamageType weakness;
    //[HideInInspector]
    public bool facePlayer;
    //[HideInInspector]
    public bool attacks;
    //[HideInInspector]
    public EnemyAI enemyAI;
    //[HideInInspector]
    public List<GameObject> enemyWaypoints;
    //[HideInInspector]
    int numWaypoints;

    GameObject player;
    bool isDead = false;

    /***
     * @author Darrick Hilburn
     * 
     * Start finds the player, sets this enemy as alive, and starts 
     *    shooting and moving if it is set to.
     */
	void Start ()
    {
        // Find the player
        player = GameObject.FindWithTag(WaypointEnumValues.PLAYER_TAG);
        isDead = false;
        
        // Start moving if AI is set to move
        if (enemyAI == EnemyAI.MOVE)
        {
            StartCoroutine("MovementEngine");
            StartCoroutine("FacingsEngine");
        }
        // Start attacking if able to attack
        if (attacks)
        {
            InvokeRepeating("ShootBullet", firstShotTime, repeatShotTime);
        }
	}
	
    /**
     * @author Darrick Hilburn
     * 
     * Update checks if the enemy should exist and if they and the
     *    player are still alive. 
     *    If the enemy dies, they are destroyed
     *    If the player dies, the enemy stops all actions
     */
	void Update ()
    {
        // Destroy this enemy if, according to its spawner, it does not exist
        if (!GetComponentInParent<EnemySpawner>().EnemyExists())
            Destroy(gameObject);

        // Tell this enemy it's dead if its health is below 0
        if (health <= 0)
        {
            isDead = true;
        }

        // Have the enemy face the player if it isn't dead and its flag says it does
        if (!isDead)
        {
            if(facePlayer)
                FacePlayer();
        }
        // Destroy this enemy if it's dead
        else
        {
            Debug.Log("Enemy has died!");
            Destroy(gameObject);
        }
        // Have this enemy stop moving and attacking if the player is dead
        if (player.GetComponent<PlayerScript>().PlayerIsDead())
        {
            StopAllCoroutines();
            CancelInvoke("ShootBullet");
        }
	}

    /**
     * @author Darrick Hilburn
     * 
     * OnTriggerEnter typically registers when a projectile hits this enemy.
     *    If the projectile damage type is the enemy weakness, this enemy takes double damage.
     *    If the projectile damage type is the enemy resistance, this enemy takes half damage.
     *    Otherwise, this enemy takes damage based on the projectile's damage parameter.
     */
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals(WaypointEnumValues.BULLET_TAG))
        {
            // Take double damage from weakness exploitation
            if (other.GetComponent<BulletScript>().damageType.Equals(weakness) && weakness != DamageType.NONE)
            {
                health -= other.GetComponent<BulletScript>().damage * 2;
                Debug.Log("Weakness exploited! Enemy took " + other.GetComponent<BulletScript>().damage + " damage!");
            }
            // Take half damage from resistance
            else if (other.GetComponent<BulletScript>().damageType.Equals(resistance) && resistance != DamageType.NONE)
            {
                health -= other.GetComponent<BulletScript>().damage / 2;
                Debug.Log("Enemy resisted! Enemy took " + other.GetComponent<BulletScript>().damage + " damage!");
            }
            // Take normal damage
            else
            {
                health -= other.GetComponent<BulletScript>().damage;
                Debug.Log("Enemy took " + other.GetComponent<BulletScript>().damage + " damage");
            }            
            Debug.Log("Enemy now has " + health + " health");
        }
    }
    
    /**
     * @author Darrick Hilburn
     * 
     * FacePlayer has this enemy constantly facing the player.
     */
    void FacePlayer()
    {
        Vector3 relativePos = player.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos);
        transform.rotation = rotation;
    }

    /**
     * @author Darrick Hilburn
     * 
     * ShootBullet has this enemy shoot bullets at the player based on the
     *    enemy's relative forward vector to its position in worldspace.
     */
    void ShootBullet()
    {
        Vector3 spawnPosition = transform.position + transform.forward;
        Instantiate(projectile, spawnPosition, transform.rotation);
    }

    #region Movements

    /**
        @authors Linda Lane, Darrick Hilburn

        MovementsEngine makes the player move between waypoints.

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator MovementEngine()
    {
        numWaypoints = enemyWaypoints.Count;
        for (int i = 0; i < numWaypoints; i++)
        {
            WaypointScript waypointData = enemyWaypoints[i].GetComponent<WaypointScript>();

            MovementType move = waypointData.moveType;
            switch (move)
            {
                case (MovementType.STRAIGHT_LINE):
                    yield return StartCoroutine("StraightLineMove", i);
                    break;
                case (MovementType.BEZIER_CURVE):
                    yield return StartCoroutine("BezierCurveMove", i);
                    break;
                default:
                    yield return new WaitForSeconds(waypointData.moveData.moveTime);
                    break;
            }
        }
        Debug.Log("End of path reached!");
        Cursor.lockState = CursorLockMode.None;
    }

    #region Movement Engine functions

    /**
        @authors Darrick Hilburn, Linda Lane

        StraightLineMove moves the player directly from one waypoint to another

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator StraightLineMove(int waypointIndex)
    {
        WaypointScript waypointData = enemyWaypoints[waypointIndex].GetComponent<WaypointScript>();

        Vector3 startPos = waypointData.moveData.startPos.transform.position;
        Vector3 endPos = waypointData.moveData.endPos.transform.position;
        float moveDelay = waypointData.moveData.moveDelay;
        float moveTime = waypointData.moveData.moveTime;

        float step = 0;
        transform.position = startPos;
        if (moveDelay > 0)
            yield return new WaitForSeconds(moveDelay);
        while (step < moveTime)
        {
            transform.position = Vector3.Lerp(startPos, endPos, (step / moveTime));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        transform.position = endPos;
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        BezierCurveMove moves the player from one point to another along a curve

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator BezierCurveMove(int waypointIndex)
    {
        WaypointScript waypointData = enemyWaypoints[waypointIndex].GetComponent<WaypointScript>();

        Vector3 startPos = waypointData.moveData.startPos.transform.position;
        Vector3 endPos = waypointData.moveData.endPos.transform.position;
        Vector3 curvePos = waypointData.moveData.curvePoint.transform.position;
        float moveDelay = waypointData.moveData.moveDelay;
        float moveTime = waypointData.moveData.moveTime;

        float step = 0;
        transform.position = startPos;
        if (moveDelay > 0)
            yield return new WaitForSeconds(moveDelay);
        while (step < moveTime)
        {
            Vector3 endPoint = GetPoint(startPos, endPos, curvePos, (step / moveTime));
            transform.position = Vector3.Lerp(transform.position, endPoint, (step / moveTime));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
            transform.position = endPoint;
        }
        transform.position = endPos;
    }

    /**
        @author Linda Lane

        GetPoint calculates the point the player will move to in the middle of a bezier curve move
    */
    Vector3 GetPoint(Vector3 startPos, Vector3 endPos, Vector3 curvePos, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1.0f - t;
        return (oneMinusT * oneMinusT * startPos) + (2.0f * oneMinusT * t * curvePos) + (t * t * endPos);
    }
    #endregion

    #endregion

    #region Facings

    /**
        @authors Darrick Hilburn, Linda Lane

        FacingsEngine changes what the player is looking at/how the player is
        looking during a move 

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator FacingsEngine()
    {
        numWaypoints = enemyWaypoints.Count;
        for (int i = 0; i < numWaypoints; i++)
        {
            WaypointScript waypointData = enemyWaypoints[i].GetComponent<WaypointScript>();
            FacingType facing = waypointData.facing;
            switch (facing)
            {
                case (FacingType.LOOK_AND_RETURN):
                    yield return StartCoroutine("LookAndReturn", i);
                    break;
                case (FacingType.LOOK_CHAIN):
                    yield return StartCoroutine("LookChain", i);
                    break;
                case (FacingType.FORCED_DIRECTION):
                    yield return StartCoroutine("ForcedLocation", i);
                    break;
                default:
                    yield return StartCoroutine("NoFacing", i);
                    break;
            }
        }
        yield return null;
    }

    #region Facing Engine functions

    /**
        @authors Darrick Hilburn, Linda Lane

        LookAndReturn makes the player turn to look at a point for a time,
        then return to their initial facing direction

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator LookAndReturn(int waypointIndex)
    {

        // The waypoint to get data from
        WaypointScript waypointData = enemyWaypoints[waypointIndex].GetComponent<WaypointScript>();

        // Target position
        Vector3 focusPoint = waypointData.faceData.focusPoints[0].transform.position;

        // Player camera initial position and facing rotation
        Vector3 startPosition = transform.position;
        Quaternion startFacing = transform.rotation;

        // Calculate rotation
        Quaternion lookRotation = Quaternion.LookRotation(focusPoint - startPosition);
        Quaternion currentRotation = startFacing;

        // Total time it takes to wait, pan, focus, and return combined
        // Does not exceed the movement time
        float maxFaceTime = waypointData.moveData.totalMoveTime;
        float elapsedFaceTime = 0;

        // First wait if needed
        float faceDelay = waypointData.faceData.faceDelay;
        yield return new WaitForSeconds(faceDelay);
        elapsedFaceTime += faceDelay;

        // Pan to focus
        float panTime = waypointData.faceData.panTimes[0];
        yield return StartCoroutine(PanFocus(startFacing, lookRotation, panTime));
        currentRotation = transform.rotation;
        elapsedFaceTime += panTime;

        // Look at focus
        float focusTime = waypointData.faceData.faceTimes[0];
        yield return StartCoroutine(HoldFocus(focusPoint, focusTime));
        currentRotation = transform.rotation;
        elapsedFaceTime += focusTime;

        // Pan back to start
        float returnTime = waypointData.faceData.returnTime;
        yield return StartCoroutine(PanFocus(currentRotation, startFacing, returnTime));
        elapsedFaceTime += returnTime;

        transform.rotation = startFacing;
        if (elapsedFaceTime < maxFaceTime)
            yield return new WaitForSeconds(maxFaceTime - elapsedFaceTime);
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        LookChain makes the player turn to look at several points for varying times,
        then return to their initial facing direction after facing all points

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator LookChain(int waypointIndex)
    {

        WaypointScript waypointData = enemyWaypoints[waypointIndex].GetComponent<WaypointScript>();

        Vector3 startPosition = transform.position;
        Quaternion startFacing = transform.rotation;

        float maxFaceTime = waypointData.moveData.totalMoveTime;
        float elapsedFaceTime = 0;

        int numFocusPoints = waypointData.faceData.numFocusPoints;

        Vector3 currentPosition = startPosition;
        Quaternion currentRotation = startFacing;

        float faceDelay = waypointData.faceData.faceDelay;
        yield return new WaitForSeconds(faceDelay);
        elapsedFaceTime += faceDelay;

        for (int i = 0; i < numFocusPoints; i++)
        {
            Vector3 focusPoint = waypointData.faceData.focusPoints[i].transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(focusPoint - currentPosition);

            float panTime = waypointData.faceData.panTimes[i];
            yield return StartCoroutine(PanFocus(currentRotation, lookRotation, panTime));
            currentPosition = transform.position;
            currentRotation = transform.rotation;
            elapsedFaceTime += panTime;

            float focusTime = waypointData.faceData.faceTimes[i];
            yield return StartCoroutine(HoldFocus(focusPoint, focusTime));
            currentPosition = transform.position;
            currentRotation = transform.rotation;
            elapsedFaceTime += focusTime;
        }
        float returnTime = waypointData.faceData.returnTime;
        yield return StartCoroutine(PanFocus(currentRotation, startFacing, returnTime));
        elapsedFaceTime += returnTime;

        transform.rotation = startFacing;
        if (elapsedFaceTime < maxFaceTime)
            yield return new WaitForSeconds(maxFaceTime - elapsedFaceTime);
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        ForcedLocation makes the player turn and face a point for a move

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator ForcedLocation(int waypointIndex)
    {

        WaypointScript waypointData = enemyWaypoints[waypointIndex].GetComponent<WaypointScript>();
        Vector3 focusPoint = waypointData.faceData.focusPoints[0].transform.position;

        Vector3 startPosition = transform.position;
        Quaternion startFacing = transform.rotation;

        Quaternion lookRotation = Quaternion.LookRotation(focusPoint - startPosition);
        Quaternion currentRotation = startFacing;

        yield return new WaitForSeconds(waypointData.faceData.faceDelay);

        float panTime = waypointData.faceData.panTimes[0];
        yield return StartCoroutine(PanFocus(startFacing, lookRotation, panTime));
        currentRotation = transform.rotation;

        float faceTime = waypointData.moveData.totalMoveTime - waypointData.faceData.faceDelay - waypointData.faceData.panTimes[0];
        yield return StartCoroutine(HoldFocus(focusPoint, faceTime));
        currentRotation = transform.rotation;

        transform.rotation = currentRotation;
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        NoFacing makes the player face a set direction, not focused on any point

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator NoFacing(int waypointIndex)
    {
        WaypointScript waypointData = enemyWaypoints[waypointIndex].GetComponent<WaypointScript>();
        yield return new WaitForSeconds(waypointData.moveData.totalMoveTime);
    }


    /**
        @author Darrick Hilburn, Linda Lane

        PanFocus makes the player focus pan from their initial focus to the target 
        over a passed time

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator PanFocus(Quaternion initialFocus, Quaternion targetFocus, float panTime)
    {
        float timeStep = 0;
        while (timeStep < panTime)
        {
            float rotateTime = timeStep / panTime;
            transform.rotation = Quaternion.Slerp(initialFocus, targetFocus, rotateTime);
            timeStep += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForEndOfFrame();
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        HoldFocus has the player focus on a point for a time

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator HoldFocus(Vector3 focusPoint, float focusTime)
    {
        float timeStep = 0;
        while (timeStep < focusTime)
        {
            transform.LookAt(focusPoint);
            timeStep += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForEndOfFrame();
    }
    #endregion

    #endregion
}