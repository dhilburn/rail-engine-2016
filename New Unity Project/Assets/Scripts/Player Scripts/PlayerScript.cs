﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEditor;

public class PlayerScript : MonoBehaviour
{
    public WeaponScript[] editorWeapons; // Array of weapons the player has. Used in editor scripts
    public List<WeaponScript> weapons;   // List of weapons the player has. Set to a list so the player can pick up weapons in-game
    public WeaponScript activeWeapon;    // The player's currently active weapon
    public int playerHealth = 100;       // Designer editable health

    

    //[HideInInspector]
    public int health;                  // Player in-game health
    [SerializeField]
    Text weaponNameLabel;               // GUI area showing active weapon name
    [SerializeField]
    Text ammoLabel;                     // GUI area showing active weapon ammo
    [SerializeField]
    Text healthLabel;                   // GUI area showing player health
    bool firingBullet = false;          // Flag for both firing and reloading a weapon
    PlayerRailEngine engine;            // The player rail engine in a scene
    bool isDead = false;                // Flag for if the player is dead

    

    // Start loads all the players weapons and sets the first weapon in the list as active
    void Start()
    {
        InitializeWeaponsList();
        InitializeWeaponAmmo();
        activeWeapon = weapons[0];
        health = playerHealth;
        engine = GameObject.FindGameObjectWithTag("Engine").GetComponent<PlayerRailEngine>();
        isDead = false;
        for (int i = 0; i < weapons.Count; i++)
        {
            Debug.Log("Game start");
            DebugWeaponsList(i);
        }
    }

    // Update checks key presses and reacts to them.
    // Left mouse click fires the active weapon, but if the weapon is empty, it reloads the active weapon.
    // Right mouse click reloads the active weapon.
    // Scrolling the mouse wheel changes the currently active weapon.
    // All mouse actions only work when the player isn't firing their weapon!
    void Update()
    {
        UpdateAmmo();
        UpdateHealth();

        if (!isDead)
        {
            if (health > 0)
            {
                if (Input.GetMouseButton(0) && !firingBullet)
                {
                    firingBullet = true;
                    if (activeWeapon.GetClipSize() > 0)
                    {
                        StartCoroutine("SpawnBullet");
                        Debug.Log("Current clip size: " + activeWeapon.GetClipSize());
                        Debug.Log("Two-thirds max clip = " + ((float)activeWeapon.maxClipSize * (2.0f/3.0f)));
                        Debug.Log("One-third max clip = " + ((float)activeWeapon.maxClipSize / 3.0f));
                    }
                    else
                        StartCoroutine("Reload");
                }

                if (Input.GetMouseButtonDown(1) && !firingBullet)
                {
                    firingBullet = true;
                    StartCoroutine("Reload");
                }

                // Change to the next weapon in the list when scrolling the mouse wheel up
                if (Input.GetAxis("Mouse ScrollWheel") > 0 && !firingBullet)
                {
                    ChangeWeapons(weapons.IndexOf(activeWeapon) + 1);
                }

                // Change to the previous weapon in the list when scrolling the mouse wheel down
                if (Input.GetAxis("Mouse ScrollWheel") < 0 && !firingBullet)
                {
                    ChangeWeapons(weapons.IndexOf(activeWeapon) - 1);
                }
            }
            else
            {                
                engine.KillEngine();
                isDead = true;
            }
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag.Equals(WaypointEnumValues.BULLET_TAG))
        {
            health -= col.gameObject.GetComponent<GameObject>().GetComponent<BulletScript>().damage;
            Debug.Log("Player took " + col.gameObject.GetComponent<BulletScript>().damage + " damage");
            Debug.Log("Player now has " + health + " health");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals(WaypointEnumValues.BULLET_TAG))
        {
            health -= other.GetComponent<BulletScript>().damage;
            Debug.Log("Player took " + other.GetComponent<BulletScript>().damage + " damage");
            Debug.Log("Player now has " + health + " health");
        }
    }

    void OnGUI()
    {
        healthLabel.text = health.ToString();
        weaponNameLabel.text = activeWeapon.name;
        ammoLabel.text = activeWeapon.GetClipSize() + " / " + activeWeapon.maxClipSize;
        if (health <= 0)
        {
            Rect gameOverRect = new Rect(Screen.width / 2, Screen.height / 2, 120, 20);
            Rect restartRect = new Rect(Screen.width / 2, Screen.height / 2 + 24, 60, 20);
            GUI.Label(gameOverRect, "YOU ARE DEAD");
            if (GUI.Button(restartRect, "Restart"))
            {
                health = playerHealth;
                isDead = false;
                engine.ResetEngine();
            }
        }

    }

    void ChangeWeapons(int activeWeaponIndex)
    {
        // If the current active weapon is first in the list,
        //    wrap around to the last weapon.
        if (activeWeaponIndex < 0)
        {
            activeWeapon = weapons[weapons.Count - 1];
        }
        // If the current active weapon is last in the list,
        //    wrap around to the first weapon.
        else if (activeWeaponIndex > weapons.Count - 1)
        {
            activeWeapon = weapons[0];
        }
        else
        {
            activeWeapon = weapons[activeWeaponIndex];
        }
    }

    public void InitializeEditorWeaponsList()
    {
        editorWeapons = new WeaponScript[2];
        GameObject cannonball = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Dynamic Assets/Resources/Prefabs/Cannonball.prefab", typeof(GameObject));
        GameObject rivet = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Dynamic Assets/Resources/Prefabs/Rivet.prefab", typeof(GameObject));
        WeaponScript cannon = new WeaponScript("Cannon", cannonball, 1500, 10, 5, 2, 2, DamageType.LAND);
        WeaponScript rivetGun = new WeaponScript("Rivet Gun", rivet, 2000, 20, 8, 3, 3, DamageType.AERIAL);
        editorWeapons[0] = cannon;
        editorWeapons[1] = rivetGun;
    }    

    public void InitializeHealthGUI()
    {
        healthLabel = GameObject.Find("Health").GetComponent<Text>();
    }

    public void InitializeWeaponsGUI()
    {
        weaponNameLabel = GameObject.Find("WeaponName").GetComponent<Text>();
        ammoLabel = GameObject.Find("Ammo").GetComponent<Text>();
    }

    public bool PlayerIsDead()
    {
        return isDead;
    }

    public void DebugWeaponsList(int index)
    {
        Debug.Log("Player weapons");
        Debug.Log("Weapon " + (index + 1));
        Debug.Log("Weapon name: " + weapons[index].name);
        Debug.Log("Weapon projectile: " + weapons[index].projectile.name);
        Debug.Log("Projectile force: " + weapons[index].bulletForce);
        Debug.Log("Damage: " + weapons[index].damage);
        Debug.Log("Max clip size: " + weapons[index].maxClipSize);
        Debug.Log("Weapon cooldown: " + weapons[index].cooldown);
        Debug.Log("Weapon reload time: " + weapons[index].reloadTime);
    }

    public static void PlayerWeaponsAssign(PlayerScript data, PlayerScript assign)
    {
        for (int i = 0; i < data.editorWeapons.Length; i++)
        {
            WeaponScript.WeaponAssign(data.editorWeapons[i], assign.editorWeapons[i]);
        }
    }

    void InitializeWeaponsList()
    {
        weapons = new List<WeaponScript>();
        for (int i = 0; i < editorWeapons.Length; i++)
            weapons.Add(editorWeapons[i]);
    }

    void InitializeWeaponAmmo()
    {
        foreach (WeaponScript weapon in weapons)
        {
            weapon.ResetClipSize(weapon.maxClipSize);
            weapon.projectile.GetComponent<BulletScript>().force = weapon.bulletForce;
            weapon.projectile.GetComponent<BulletScript>().damage = weapon.damage;
            weapon.projectile.GetComponent<BulletScript>().damageType = weapon.damageType;
        }
    }

    void UpdateHealth()
    {
        float currentHealth = (float)health;
        float twoThirdsHealth = (float)playerHealth * (2.0f / 3.0f);
        float oneThirdHealth = (float)playerHealth / 3.0f;

        if (currentHealth >= twoThirdsHealth)
            healthLabel.color = Color.green;
        else if (currentHealth < twoThirdsHealth && currentHealth > oneThirdHealth)
            healthLabel.color = Color.yellow;
        else if (currentHealth < oneThirdHealth && currentHealth > 0)
            healthLabel.color = Color.red;
        else
            healthLabel.color = Color.black;
    }    

    void UpdateAmmo()
    {
        float currentAmmo = (float)activeWeapon.GetClipSize();
        float twoThirdsClip = (float)activeWeapon.maxClipSize * (2.0f / 3.0f);
        float oneThirdClip = (float)activeWeapon.maxClipSize / 3.0f;

        if (currentAmmo >= twoThirdsClip)
            ammoLabel.color = Color.green;
        else if (currentAmmo < twoThirdsClip && currentAmmo >= oneThirdClip)
            ammoLabel.color = Color.yellow;
        else if (currentAmmo < oneThirdClip && currentAmmo > 0)
            ammoLabel.color = Color.red;
        else
            ammoLabel.color = Color.black;
    }    

    // Spawn bullet spawns the projectile attached to the active weapon based on the player's location and facing.
    // The player must wait until the cooldown timer on the weapon has gone off to act again.
    IEnumerator SpawnBullet()
    {
        //Vector3 spawnPosition = GameObject.FindGameObjectWithTag(WaypointEnumValues.PLAYER_BULLET_SPAWN_TAG).transform.position;
        Vector3 spawnPosition = Input.mousePosition;
        spawnPosition.z = 2.5f;
        spawnPosition = Camera.main.ScreenToWorldPoint(spawnPosition);
        //Vector3 spawnOffset = GameObject.FindGameObjectWithTag(WaypointEnumValues.PLAYER_BULLET_SPAWN_TAG).transform.forward;
        Quaternion spawnFacing = GameObject.FindGameObjectWithTag(WaypointEnumValues.PLAYER_BULLET_SPAWN_TAG).transform.rotation;
        Instantiate(activeWeapon.projectile, spawnPosition, spawnFacing);
        activeWeapon.FireBullet();
        yield return new WaitForSeconds(activeWeapon.cooldown);
        firingBullet = false;
    }

    // Reload reloads the active weapon to max capacity.
    // The player must wait until the reload timer on the weapon has gone off to act again.
    IEnumerator Reload()
    {
        yield return new WaitForSeconds(activeWeapon.reloadTime);
        activeWeapon.ResetClipSize(activeWeapon.maxClipSize);
        firingBullet = false;
    }

    public WeaponScript[] IncreaseWeaponsArraySize(int newSize)
    {
        WeaponScript[] newWeaponsArray = new WeaponScript[newSize];
        for (int i = 0; i < newWeaponsArray.Length; i++)
            newWeaponsArray[i] = new WeaponScript();

        for (int i = 0; i < editorWeapons.Length; i++)
            WeaponScript.WeaponAssign(editorWeapons[i], newWeaponsArray[i]);
        newWeaponsArray[newSize - 1] = new WeaponScript();

        return newWeaponsArray;
    }

    public WeaponScript[] DecreaseWeaponsArraySize(int newSize, int removeWeaponIndex)
    {
        WeaponScript[] newWeaponsArray = new WeaponScript[newSize];
        for (int i = 0; i < newWeaponsArray.Length; i++)
            newWeaponsArray[i] = new WeaponScript();


        for (int i = 0, j = 0; i < newSize; i++, j++)
        {
            if (j == removeWeaponIndex)
                j++;
            WeaponScript.WeaponAssign(editorWeapons[j], newWeaponsArray[i]);
        }

        return newWeaponsArray;
    }
}
