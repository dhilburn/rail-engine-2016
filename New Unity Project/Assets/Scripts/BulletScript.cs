﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour
{
    //[HideInInspector]
    public float force;
    //[HideInInspector]
    public DamageType damageType;
    //[HideInInspector]
    public int damage;

	// Use this for initialization
	void Start ()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * force);
        Invoke("DespawnBullet", 1.0f);
	}
	
    void OnCollisionEnter(Collision collision)
    {
        DespawnBullet();
    }

	void OnTriggerEnter(Collider other)
    {
        DespawnBullet();
    } 

    void DespawnBullet()
    {
        Destroy(gameObject);
    }
}
