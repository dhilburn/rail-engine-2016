﻿using UnityEngine;
using System.Collections.Generic;

/**
    @author Darrick hilburn

    FacingScript holds all the facing data at a waypoint.
*/
[System.Serializable]
public class FacingScript
{
    public int numFocusPoints;              // How many focus points to look at at this facing
    public List<GameObject> focusPoints;    // List of focus points to look at
    public float faceDelay;                 // Wait time before facing
    public List<float> panTimes;          // Time to go from current facing to next focus point
    public List<float> faceTimes;            // Time to perform the facing for each focus point
    public float returnTime;                // Time to return from last focus point to initial facing
    public float totalFaceTime;              // Total time spent making the facing

    /**
        The FacingScript constructor creates default delay and return times and no focus points.
        These are initialized because the constructor is ONLY called when a new waypoint is created,
        which defaults facing to a free look.
    */
    public FacingScript()
    {
        numFocusPoints = 0;
        faceDelay = 0;
        returnTime = 0;
    }
}
