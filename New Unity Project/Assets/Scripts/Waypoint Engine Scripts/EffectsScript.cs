﻿using UnityEngine;
using UnityEngine.UI;

/**
    @author Darrick Hilburn

    EffectsScript holds effect data for an effect at a waypoint.
*/
[System.Serializable]
public class EffectsScript
{
    public EffectType effectType;       // Type of effect to perform
    public float effectDelay;           // Time to wait before performing effect
    public float effectDuration;        // Effect duration time

    public float shakeIntensity;        // How hard the camera will shake

    public FadeType fadeType;           // What kind of fade to perform for camera fade or splatter
    public float fadeInTime;            // Time it takes to fade in
    public float fadeOutTime;           // Time it takes to fade out
    public Color fadeColor;             // Color to fade to
    
    public bool splatterFade;           // Flag for if the splatter effect will fade 

    /**
        The EffectsScript constructor defaults to an effect wait.
        This is initialized as such because the constructor is ONLY called when a waypoint
        is given a new effect.
    */
    public EffectsScript()
    {
        effectType = EffectType.WAIT;
    }
}
