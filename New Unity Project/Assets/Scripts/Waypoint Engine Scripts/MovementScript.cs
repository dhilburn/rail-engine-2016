﻿using UnityEngine;

/**
    @author Darrick Hilburn

    MovementScript holds the movement data at a waypoint.
*/
[System.Serializable]
public class MovementScript
{
    public GameObject startPos;        // Starting position for the move
    public GameObject endPos;          // Ending position for the move
    public GameObject curvePoint;      // Point a bezier curve move is influenced by
    public float moveDelay;            // Time to wait before moving
    public float moveTime;             // Time it takes to perform the move
    public float totalMoveTime;        // Total time to make the move: delay + moveTime

    /**
        @author Darrick Hilburn

        The MovementScript constructor initializes move times.
        Only move times are initialized because the constructor is ONLY called
        when a new waypoint is created, which defaults to a wait move.
    */
    public MovementScript()
    {
        moveDelay = 0;
        moveTime = 5;
        totalMoveTime = moveDelay + moveTime;
    }
}
