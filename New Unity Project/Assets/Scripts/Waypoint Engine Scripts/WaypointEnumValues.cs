﻿/**
 * @author Darrick Hilburn
 * 
 * Common enum types and constants used throughout this project
 * are put in this script.
 */
public enum WaypointType
{
    MOVEMENT,
    FOCUS,
    CURVE
}

public enum MovementType
{
    STRAIGHT_LINE,
    BEZIER_CURVE,
    WAIT
}

public enum FacingType
{
    FREE_LOOK,
    WAIT,
    FORCED_DIRECTION,
    LOOK_AND_RETURN,
    LOOK_CHAIN
}

public enum EffectType
{
    CAMERA_SHAKE,
    CAMERA_FADE,
    CAMERA_SPLATTER,
    WAIT
}

public enum FadeType
{
    FADE_OUT,
    FADE_IN,
    FADE_IN_THEN_OUT,
    FADE_OUT_THEN_IN
}

public enum DamageType
{
    NONE,
    LAND,
    AERIAL
}

public enum EnemyAI
{
    STAND,
    MOVE
}

public static class WaypointEnumValues
{
    public const string WAYPOINT_TAG = "Waypoint";
    public const string ENEMY_WAYPOINT_TAG = "Enemy Waypoint";
    public const string FOCUS_TAG = "Focus Point";
    public const string CURVE_TAG = "Curve Point";
    public const string PLAYER_TAG = "Player";

    public const string ENEMY_TAG = "Enemy";
    public const string ENEMY_SPAWN_TAG = "Enemy Spawner";
    public const string ENEMY_SPAWN_CONTAINER = "Enemy Spawner Container";

    public const string BULLET_TAG = "Bullet";
    public const string PLAYER_BULLET_SPAWN_TAG = "Player Bullet Spawner";
    public const string ENEMY_BULLET_SPAWN_TAG = "Enemy Bullet Spawner";

    public const string FADE_IMAGE_TAG = "Fade Image";
    public const string SPLATTER_IMAGE_TAG = "Splatter Image";

    public const float MAX_ALPHA = 255;

    public const float MAX_DELAY_TIME = 20f;
    public const float MAX_MOVE_TIME = 60f;
}