﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 *  @author Darrick Hilburn
    
    WaypointScript holds all the movement, facing, and effects data for the engine
 */
public class WaypointScript : MonoBehaviour 
{
    [HideInInspector]
    public WaypointType waypointType;      // The type of this waypoint
    [HideInInspector]
    public MovementType moveType;           // For a movement waypoint, the type of move to make at this waypoint
    [HideInInspector]
    public FacingType facing;               // For a movement waypoint, the type of facing to make at this waypoint
    [HideInInspector]
    public int numEffects;                  // The number of effects to trigger at this waypoint

    [HideInInspector]
    public MovementScript moveData;         // The movement data for this waypoint
    [HideInInspector]
    public FacingScript faceData;           // The facing data for this waypoint    
    [HideInInspector]
    public List<EffectsScript> effectsData; // The effects data for this waypoint

    public void InitializeWaypoint()
    {
        waypointType = WaypointType.MOVEMENT;
        moveType = MovementType.WAIT;
        facing = FacingType.FREE_LOOK;
        numEffects = 0;
        moveData = new MovementScript();
        faceData = new FacingScript();
    }    

    public void InitializeWaypoint(WaypointType type, MovementType move, FacingType face)
    {
        waypointType = type;
        moveType = move;
        facing = face;
        numEffects = 0;
        moveData = new MovementScript();
        faceData = new FacingScript();
    }
}
