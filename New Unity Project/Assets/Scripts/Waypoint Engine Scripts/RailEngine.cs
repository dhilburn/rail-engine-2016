﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// THIS SCRIPT IS NOW OBSOLETE.
/// THE PLAYERRAILENGINE SCRIPT IS USED FOR THE CORE RAIL ENGINE
/// </summary>
public class RailEngine : MonoBehaviour 
{
    public GameObject follower;
    public Camera playerCam;
    public List<GameObject> waypoints;    
    public int lastPoint = 0;

    [SerializeField]
    MouseLook mouseLookScript;
    int numWaypoints;
    GameObject effectsCanvas;
    

	// Use this for initialization
	void Start () 
    {
        //player = GameObject.FindWithTag(WaypointEnumValues.PLAYER_TAG);

        if (follower.tag.Equals(WaypointEnumValues.PLAYER_TAG))
        {
            playerCam = follower.GetComponentInChildren<Camera>();
            mouseLookScript = follower.GetComponentInChildren<MouseLook>();
            mouseLookScript.enabled = false;
        }
        else
            playerCam = null;
        follower.transform.position = waypoints[0].transform.position;

        foreach (GameObject waypoint in waypoints)
        {
            waypoint.GetComponent<WaypointScript>().moveData.totalMoveTime = waypoint.GetComponent<WaypointScript>().moveData.moveDelay + waypoint.GetComponent<WaypointScript>().moveData.moveTime;
        }

        Cursor.lockState = CursorLockMode.Locked;
        StartCoroutine("MovementEngine");
        StartCoroutine("FacingsEngine");
	}

    #region Movements

    IEnumerator MovementEngine()
    {
        numWaypoints = waypoints.Count;
        for (int i = 0; i < numWaypoints; i++)
        {
            WaypointScript waypointData = waypoints[i].GetComponent<WaypointScript>();
            MovementType move = waypointData.moveType;
            switch (move)
            {
                case(MovementType.STRAIGHT_LINE):
                    yield return StartCoroutine("StraightLineMove", i);
                    break;
                case(MovementType.BEZIER_CURVE):
                    yield return StartCoroutine("BezierCurveMove", i);
                    break;
                default:
                    yield return new WaitForSeconds(waypointData.moveData.moveTime);
                    break;
            }            
        }
        Debug.Log("End of path reached!");
        Cursor.lockState = CursorLockMode.None;
    }

    #region Movement Engine functions
    IEnumerator StraightLineMove(int waypointIndex)
    {
        Vector3 startPos = waypoints[waypointIndex].GetComponent<WaypointScript>().moveData.startPos.transform.position;
        Vector3 endPos = waypoints[waypointIndex].GetComponent<WaypointScript>().moveData.endPos.transform.position;
        float moveDelay = waypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay;
        float moveTime = waypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime;

        float step = 0;
        follower.transform.position = startPos;
        if (moveDelay > 0)
            yield return new WaitForSeconds(moveDelay);
        while (step < moveTime)
        {
            follower.transform.position = Vector3.Lerp(startPos, endPos, (step / moveTime));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        follower.transform.position = endPos;
    }

    IEnumerator BezierCurveMove(int waypointIndex)
    {
        Vector3 startPos = waypoints[waypointIndex].GetComponent<WaypointScript>().moveData.startPos.transform.position;
        Vector3 endPos = waypoints[waypointIndex].GetComponent<WaypointScript>().moveData.endPos.transform.position;
        Vector3 curvePos = waypoints[waypointIndex].GetComponent<WaypointScript>().moveData.curvePoint.transform.position;
        float moveDelay = waypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveDelay;
        float moveTime = waypoints[waypointIndex].GetComponent<WaypointScript>().moveData.moveTime;

        float step = 0;
        follower.transform.position = startPos;
        if (moveDelay > 0)
            yield return new WaitForSeconds(moveDelay);
        while (step < moveTime)
        {
            Vector3 endPoint = GetPoint(startPos, endPos, curvePos, (step / moveTime));
            follower.transform.position = Vector3.Lerp(follower.transform.position, endPoint, (step / moveTime));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
            follower.transform.position = endPoint;
        }
        follower.transform.position = endPos;
    }

    Vector3 GetPoint(Vector3 startPos, Vector3 endPos, Vector3 curvePos, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1.0f - t;
        return (oneMinusT * oneMinusT * startPos) + (2.0f * oneMinusT * t * curvePos) + (t * t * endPos);
    }
    #endregion

    #endregion

    #region Facings

    IEnumerator FacingsEngine()
    {
        numWaypoints = waypoints.Count;
        for (int i = 0; i < numWaypoints; i++)
        {
            WaypointScript waypointData = waypoints[i].GetComponent<WaypointScript>();
            FacingType facing = waypointData.facing;
            switch (facing)
            {
                case(FacingType.LOOK_AND_RETURN):
                    mouseLookScript.enabled = false;
                    StartCoroutine("LookAndReturn", i);
                    break;
                case(FacingType.LOOK_CHAIN):
                    mouseLookScript.enabled = false;

                    break;
                case(FacingType.FORCED_DIRECTION):
                    mouseLookScript.enabled = false;

                    break;
                case(FacingType.WAIT):
                    mouseLookScript.enabled = false;
                    break;
                default:
                    mouseLookScript.enabled = true;
                    break;
            }
        }
        yield return null;
    }

    IEnumerator LookAndReturn(int waypointIndex)
    {
        // The waypoint to get data from
        WaypointScript waypointData = waypoints[waypointIndex].GetComponent<WaypointScript>();

        // Total time it takes to wait, pan, focus, and return combined
        // Does not exceed the movement time
        float maxFaceTime = waypointData.moveData.totalMoveTime;

        // Player camera initial position and facing rotation
        Vector3 startPosition = playerCam.transform.position;
        Quaternion startFacing = playerCam.transform.rotation;

        // Target position
        Vector3 focusPoint = waypointData.faceData.focusPoints[0].transform.position;

        // Calculate rotation
        Quaternion lookRotation = Quaternion.LookRotation(focusPoint - startPosition);

        mouseLookScript.enabled = false;

        // First wait if needed
        float faceDelay = waypointData.faceData.faceDelay;
        yield return new WaitForSeconds(faceDelay);

        // Pan to focus
        float timeStep = 0;
        float panTime = waypointData.faceData.panTimes[0];
        while (timeStep < panTime)
        {
            float rotateTime = timeStep / panTime;
            lookRotation = Quaternion.LookRotation(focusPoint - startPosition);
            playerCam.transform.rotation = Quaternion.Slerp(startFacing, lookRotation, rotateTime);
            timeStep += Time.deltaTime;
            yield return new WaitForEndOfFrame();            
        }

        // Look at focus
        timeStep = 0;
        float focusTime = waypointData.faceData.faceTimes[0];
        while (timeStep < focusTime)
        {
            playerCam.transform.LookAt(focusPoint);
            timeStep += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        // Pan back to start
        timeStep = 0;
        float returnTime = waypointData.faceData.returnTime;
        Quaternion currentRotation = playerCam.transform.rotation;
        while (timeStep < returnTime)
        {
            float rotateTime = timeStep / returnTime;
            playerCam.transform.rotation = Quaternion.Slerp(currentRotation, startFacing, rotateTime);
            timeStep += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        playerCam.transform.rotation = startFacing;
        float elapsedFaceTime = faceDelay + panTime + focusTime + returnTime;
        if (elapsedFaceTime < maxFaceTime)
            yield return new WaitForSeconds(maxFaceTime - elapsedFaceTime);
    }

    #endregion

    public void LoadNextLevel()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
    }
}
