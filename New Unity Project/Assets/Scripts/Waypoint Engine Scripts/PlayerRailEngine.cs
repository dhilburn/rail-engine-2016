﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerRailEngine : MonoBehaviour 
{
    public List<GameObject> waypoints;

    public GameObject player;
    public Camera playerCam;
    [SerializeField]
    MouseLook mouseLookScript;   

    [SerializeField]
    Image splatter;
    [SerializeField]
    Image fader;

    [SerializeField]
    Texture2D cursor;
    int numWaypoints;

    /**
        @author Darrick Hilburn

        Start gets the player and waypoints, then starts all engine coroutines
    */
    void Start()
    {
        player = GameObject.FindWithTag(WaypointEnumValues.PLAYER_TAG);
        playerCam = player.GetComponentInChildren<Camera>();
        mouseLookScript = player.GetComponentInChildren<MouseLook>();
        player.transform.position = waypoints[0].transform.position;        

        foreach (GameObject waypoint in waypoints)
        {
            waypoint.GetComponent<WaypointScript>().moveData.totalMoveTime = waypoint.GetComponent<WaypointScript>().moveData.moveDelay + waypoint.GetComponent<WaypointScript>().moveData.moveTime;
        }
        

        Cursor.lockState = CursorLockMode.Confined;
        //Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
        

        StartCoroutine("MovementEngine");
        StartCoroutine("FacingsEngine");
        StartCoroutine("EffectsEngine");
    }

    #region Movements

    /**
        @authors Linda Lane, Darrick Hilburn

        MovementsEngine makes the player move between waypoints.

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator MovementEngine()
    {
        numWaypoints = waypoints.Count;
        for (int i = 0; i < numWaypoints; i++)
        {
            WaypointScript waypointData = waypoints[i].GetComponent<WaypointScript>();

            MovementType move = waypointData.moveType;
            switch (move)
            {
                case (MovementType.STRAIGHT_LINE):
                    yield return StartCoroutine("StraightLineMove", i);
                    break;
                case (MovementType.BEZIER_CURVE):
                    yield return StartCoroutine("BezierCurveMove", i);
                    break;
                default:
                    yield return new WaitForSeconds(waypointData.moveData.moveTime);
                    break;
            }
        }
        Debug.Log("End of path reached!");
        Cursor.lockState = CursorLockMode.None;
    }

    #region Movement Engine functions
    /**
        @authors Darrick Hilburn, Linda Lane

        StraightLineMove moves the player directly from one waypoint to another

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator StraightLineMove(int waypointIndex)
    {
        WaypointScript waypointData = waypoints[waypointIndex].GetComponent<WaypointScript>();

        Vector3 startPos = waypointData.moveData.startPos.transform.position;
        Vector3 endPos = waypointData.moveData.endPos.transform.position;
        float moveDelay = waypointData.moveData.moveDelay;
        float moveTime = waypointData.moveData.moveTime;

        float step = 0;
        player.transform.position = startPos;
        if (moveDelay > 0)
            yield return new WaitForSeconds(moveDelay);
        while (step < moveTime)
        {
            player.transform.position = Vector3.Lerp(startPos, endPos, (step / moveTime));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        player.transform.position = endPos;
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        BezierCurveMove moves the player from one point to another along a curve

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator BezierCurveMove(int waypointIndex)
    {
        WaypointScript waypointData = waypoints[waypointIndex].GetComponent<WaypointScript>();

        Vector3 startPos = waypointData.moveData.startPos.transform.position;
        Vector3 endPos = waypointData.moveData.endPos.transform.position;
        Vector3 curvePos = waypointData.moveData.curvePoint.transform.position;
        float moveDelay = waypointData.moveData.moveDelay;
        float moveTime = waypointData.moveData.moveTime;

        float step = 0;
        player.transform.position = startPos;
        if (moveDelay > 0)
            yield return new WaitForSeconds(moveDelay);
        while (step < moveTime)
        {
            Vector3 endPoint = GetPoint(startPos, endPos, curvePos, (step / moveTime));
            player.transform.position = Vector3.Lerp(player.transform.position, endPoint, (step / moveTime));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
            player.transform.position = endPoint;
        }
        player.transform.position = endPos;
    }

    /**
        @author Linda Lane

        GetPoint calculates the point the player will move to in the middle of a bezier curve move
    */
    Vector3 GetPoint(Vector3 startPos, Vector3 endPos, Vector3 curvePos, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1.0f - t;
        return (oneMinusT * oneMinusT * startPos) + (2.0f * oneMinusT * t * curvePos) + (t * t * endPos);
    }
    #endregion

    #endregion

    #region Facings

    /**
        @authors Darrick Hilburn, Linda Lane

        FacingsEngine changes what the player is looking at/how the player is
        looking during a move 

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator FacingsEngine()
    {
        numWaypoints = waypoints.Count;
        for (int i = 0; i < numWaypoints; i++)
        {
            WaypointScript waypointData = waypoints[i].GetComponent<WaypointScript>();
            FacingType facing = waypointData.facing;
            switch (facing)
            {
                case (FacingType.LOOK_AND_RETURN):
                    yield return StartCoroutine("LookAndReturn", i);
                    break;
                case (FacingType.LOOK_CHAIN):
                    yield return StartCoroutine("LookChain", i);
                    break;
                case (FacingType.FORCED_DIRECTION):
                    yield return StartCoroutine("ForcedLocation", i);
                    break;
                case (FacingType.WAIT):
                    yield return StartCoroutine("NoFacing", i);
                    break;
                default:
                    yield return StartCoroutine("FreeLook", i);
                    break;
            }
        }
        yield return null;
    }

    #region Facing Engine functions

    /**
        @authors Darrick Hilburn, Linda Lane

        LookAndReturn makes the player turn to look at a point for a time,
        then return to their initial facing direction

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator LookAndReturn(int waypointIndex)
    {
        mouseLookScript.enabled = false;

        // The waypoint to get data from
        WaypointScript waypointData = waypoints[waypointIndex].GetComponent<WaypointScript>();

        // Target position
        Vector3 focusPoint = waypointData.faceData.focusPoints[0].transform.position;

        // Player camera initial position and facing rotation
        Vector3 startPosition = playerCam.transform.position;
        Quaternion startFacing = playerCam.transform.rotation;

        // Calculate rotation
        Quaternion lookRotation = Quaternion.LookRotation(focusPoint - startPosition);
        Quaternion currentRotation = startFacing;

        // Total time it takes to wait, pan, focus, and return combined
        // Does not exceed the movement time
        float maxFaceTime = waypointData.moveData.totalMoveTime;
        float elapsedFaceTime = 0;        

        // First wait if needed
        float faceDelay = waypointData.faceData.faceDelay;
        yield return new WaitForSeconds(faceDelay);
        elapsedFaceTime += faceDelay;

        // Pan to focus
        float panTime = waypointData.faceData.panTimes[0];
        yield return StartCoroutine(PanFocus(startFacing, lookRotation, panTime));
        currentRotation = playerCam.transform.rotation;
        elapsedFaceTime += panTime;

        // Look at focus
        float focusTime = waypointData.faceData.faceTimes[0];
        yield return StartCoroutine(HoldFocus(focusPoint, focusTime));
        currentRotation = playerCam.transform.rotation;
        elapsedFaceTime += focusTime;

        // Pan back to start
        float returnTime = waypointData.faceData.returnTime;
        yield return StartCoroutine(PanFocus(currentRotation, startFacing, returnTime));
        elapsedFaceTime += returnTime;        

        playerCam.transform.rotation = startFacing;
        if (elapsedFaceTime < maxFaceTime)
            yield return new WaitForSeconds(maxFaceTime - elapsedFaceTime);
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        LookChain makes the player turn to look at several points for varying times,
        then return to their initial facing direction after facing all points

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator LookChain(int waypointIndex)
    {
        mouseLookScript.enabled = false;

        WaypointScript waypointData = waypoints[waypointIndex].GetComponent<WaypointScript>();        
        
        Vector3 startPosition = playerCam.transform.position;
        Quaternion startFacing = playerCam.transform.rotation;        

        float maxFaceTime = waypointData.moveData.totalMoveTime;
        float elapsedFaceTime = 0;

        int numFocusPoints = waypointData.faceData.numFocusPoints;

        Vector3 currentPosition = startPosition;
        Quaternion currentRotation = startFacing;

        float faceDelay = waypointData.faceData.faceDelay;
        yield return new WaitForSeconds(faceDelay);
        elapsedFaceTime += faceDelay;

        for (int i = 0; i < numFocusPoints; i++)
        {
            Vector3 focusPoint = waypointData.faceData.focusPoints[i].transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(focusPoint - currentPosition);

            float panTime = waypointData.faceData.panTimes[i];
            yield return StartCoroutine(PanFocus(currentRotation, lookRotation, panTime));
            currentPosition = playerCam.transform.position;
            currentRotation = playerCam.transform.rotation;
            elapsedFaceTime += panTime;

            float focusTime = waypointData.faceData.faceTimes[i];
            yield return StartCoroutine(HoldFocus(focusPoint, focusTime));
            currentPosition = playerCam.transform.position;
            currentRotation = playerCam.transform.rotation;
            elapsedFaceTime += focusTime;            
        }
        float returnTime = waypointData.faceData.returnTime;
        yield return StartCoroutine(PanFocus(currentRotation, startFacing, returnTime));
        elapsedFaceTime += returnTime;

        playerCam.transform.rotation = startFacing;
        if (elapsedFaceTime < maxFaceTime)
            yield return new WaitForSeconds(maxFaceTime - elapsedFaceTime);
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        ForcedLocation makes the player turn and face a point for a move

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator ForcedLocation(int waypointIndex)
    {
        mouseLookScript.enabled = false;

        WaypointScript waypointData = waypoints[waypointIndex].GetComponent<WaypointScript>();
        Vector3 focusPoint = waypointData.faceData.focusPoints[0].transform.position;

        Vector3 startPosition = playerCam.transform.position;
        Quaternion startFacing = playerCam.transform.rotation;

        Quaternion lookRotation = Quaternion.LookRotation(focusPoint - startPosition);
        Quaternion currentRotation = startFacing;        

        yield return new WaitForSeconds(waypointData.faceData.faceDelay);

        float panTime = waypointData.faceData.panTimes[0];
        yield return StartCoroutine(PanFocus(startFacing, lookRotation, panTime));
        currentRotation = playerCam.transform.rotation;

        float faceTime = waypointData.moveData.totalMoveTime - waypointData.faceData.faceDelay - waypointData.faceData.panTimes[0];
        yield return StartCoroutine(HoldFocus(focusPoint, faceTime));
        currentRotation = playerCam.transform.rotation;

        playerCam.transform.rotation = currentRotation;
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        NoFacing makes the player face a set direction, not focused on any point

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator NoFacing(int waypointIndex)
    {
        mouseLookScript.enabled = false;
        WaypointScript waypointData = waypoints[waypointIndex].GetComponent<WaypointScript>();
        yield return new WaitForSeconds(waypointData.moveData.totalMoveTime);
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        FreeLook lets the player face any direction they like

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator FreeLook(int waypointIndex)
    {
        mouseLookScript.enabled = true;
        WaypointScript waypointData = waypoints[waypointIndex].GetComponent<WaypointScript>();
        Cursor.lockState = CursorLockMode.Locked;
        if(!Cursor.visible)
            Cursor.visible = true;
        yield return new WaitForSeconds(waypointData.moveData.totalMoveTime);
        Cursor.lockState = CursorLockMode.Confined;
    }

    /**
        @author Darrick Hilburn, Linda Lane

        PanFocus makes the player focus pan from their initial focus to the target 
        over a passed time

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator PanFocus(Quaternion initialFocus, Quaternion targetFocus, float panTime)
    {
        float timeStep = 0;
        while (timeStep < panTime)
        {
            float rotateTime = timeStep / panTime;
            playerCam.transform.rotation = Quaternion.Slerp(initialFocus, targetFocus, rotateTime);
            timeStep += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForEndOfFrame();
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        HoldFocus has the player focus on a point for a time

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator HoldFocus(Vector3 focusPoint, float focusTime)
    {
        float timeStep = 0;
        while (timeStep < focusTime)
        {
            playerCam.transform.LookAt(focusPoint);
            timeStep += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForEndOfFrame();
    }
    #endregion

    #endregion

    #region Effects

    /**
        @authors Darrick Hilburn, Linda Lane

        EffectsEngine creates effects during a movement

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator EffectsEngine()
    {
        for (int i = 0; i < waypoints.Count; i++)
        {
            WaypointScript waypoint = waypoints[i].GetComponent<WaypointScript>();
            int numEffects = waypoint.numEffects;
            float elapsedEffectTime = 0;

            if (numEffects == 0)
            {
                Debug.Log("No effects found at " + waypoint.gameObject.name);
                yield return new WaitForSeconds(waypoint.moveData.totalMoveTime);
            }
            else
            {
                Debug.Log(numEffects.ToString() + " effects found at " + waypoint.gameObject.name);
                for (int j = 0; j < numEffects; j++)
                {
                    EffectType effectType = waypoint.effectsData[j].effectType;
                    switch (effectType)
                    {
                        case (EffectType.CAMERA_SHAKE):
                            yield return StartCoroutine(CameraShake(waypoint, j));
                            break;
                        case (EffectType.CAMERA_FADE):
                            yield return StartCoroutine(FadeCamera(waypoint, j));
                            break;
                        case (EffectType.CAMERA_SPLATTER):
                            yield return StartCoroutine(Splatter(waypoint, j));
                            break;
                        default:
                            yield return StartCoroutine(EffectsWait(waypoint, j));
                            break;
                    }
                    elapsedEffectTime += waypoint.effectsData[j].effectDelay + waypoint.effectsData[j].effectDuration + waypoint.effectsData[j].fadeInTime + waypoint.effectsData[j].fadeOutTime;
                }
                if (elapsedEffectTime < waypoint.moveData.totalMoveTime)
                    yield return new WaitForSeconds(waypoint.moveData.totalMoveTime - elapsedEffectTime);
            }
        }
    }

    #region Effects Engine functions

    /**
        @authors Darrick Hilburn, Linda Lane

        CameraShake shakes the camera at an intensity for a time

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator CameraShake(WaypointScript waypoint, int effectIndex)
    {
        float delayTime = waypoint.effectsData[effectIndex].effectDelay;
        float shakeTime = waypoint.effectsData[effectIndex].effectDuration;
        float shakeIntensity = waypoint.effectsData[effectIndex].shakeIntensity;

        Vector3 initialCameraPosition = playerCam.transform.localPosition;

        yield return new WaitForSeconds(delayTime);

        float timeStep = 0;
        Debug.Log("Starting camera shake");
        while (timeStep < shakeTime)
        {
            playerCam.transform.localPosition = (Random.insideUnitSphere * shakeIntensity) + initialCameraPosition;
            timeStep += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        Debug.Log("Finished shaking");
        playerCam.transform.localPosition = initialCameraPosition;
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        Fade camera makes the camera fade in or out to a specified color.

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator FadeCamera(WaypointScript waypoint, int effectIndex)
    {
        float delayTime = waypoint.effectsData[effectIndex].effectDelay;
        float fadeLinger = waypoint.effectsData[effectIndex].effectDuration;

        FadeType fadeType = waypoint.effectsData[effectIndex].fadeType;
        float fadeInTime = waypoint.effectsData[effectIndex].fadeInTime;
        float fadeOutTime = waypoint.effectsData[effectIndex].fadeOutTime;        
        Color fadeColor = waypoint.effectsData[effectIndex].fadeColor;
        switch (fadeType)
        {
            case(FadeType.FADE_OUT):           // Fade screen to color
            case (FadeType.FADE_OUT_THEN_IN):  // Fade screen to color, then back to game
                fadeColor.a = 0.0f;
                break;
            case(FadeType.FADE_IN):           // Fade screen to game
            case(FadeType.FADE_IN_THEN_OUT):  // Fade screen to game, then back to color
                fadeColor.a = 1.0f;
                break;
        }

        fader.color = fadeColor;

        yield return new WaitForSeconds(delayTime);
        Debug.Log("Fading camera");
        switch (fadeType)
        {
            case (FadeType.FADE_OUT): // Fade screen to color (invisible to visible)
                yield return StartCoroutine(SetupFade(fadeOutTime, true, fader));
                break;
            case (FadeType.FADE_IN): // Fade screen to game (visible to invisible)
                yield return StartCoroutine(SetupFade(fadeInTime, false, fader));
                break;
            case (FadeType.FADE_OUT_THEN_IN): // Fade screen to color, then back to game
                yield return StartCoroutine(SetupFade(fadeOutTime, true, fader));
                yield return new WaitForSeconds(fadeLinger);
                yield return StartCoroutine(SetupFade(fadeInTime, false, fader));
                break;
            case (FadeType.FADE_IN_THEN_OUT): // Fade screen to game, then back to color
                yield return StartCoroutine(SetupFade(fadeInTime, false, fader));
                yield return new WaitForSeconds(fadeLinger);
                yield return StartCoroutine(SetupFade(fadeOutTime, true, fader));
                break;
        }
        Debug.Log("Finished fading");
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        Splatter makes a splatter effect appear on the screen.

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator Splatter(WaypointScript waypoint, int effectIndex)
    {
        float delayTime = waypoint.effectsData[effectIndex].effectDelay;
        float splatterLingerTime = waypoint.effectsData[effectIndex].effectDuration;

        bool splatterWillFade = waypoint.effectsData[effectIndex].splatterFade;
        float fadeInTime = waypoint.effectsData[effectIndex].fadeInTime;
        float fadeOutTime = waypoint.effectsData[effectIndex].fadeOutTime;

        yield return new WaitForSeconds(delayTime);

        Color instantColor = splatter.color;
        instantColor.a = 0.0f;
        splatter.color = instantColor;

        Debug.Log("Showing splatter");
        if (splatterWillFade)
        {
            yield return StartCoroutine(SetupFade(fadeInTime, true, splatter));
        }
        else
        {
            instantColor.a = 1.0f;
            splatter.color = instantColor;
        }

        yield return new WaitForSeconds(splatterLingerTime);

        if (splatterWillFade)
        {
            yield return StartCoroutine(SetupFade(fadeOutTime, false, splatter));
        }
        else
        {
            instantColor.a = 0.0f;
            splatter.color = instantColor;
        }
        Debug.Log("Splatter cleaned");
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        EffectsWait runs no effects; recommended to use between effects

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator EffectsWait(WaypointScript waypoint, int effectIndex)
    {
        Debug.Log("Waiting until next effect");
        yield return new WaitForSeconds(waypoint.effectsData[effectIndex].effectDuration);
        Debug.Log("Wait complete");
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        SetupFade determines whether to fade in or out and then calls the fade
        coroutine to fade the passed image

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator SetupFade(float fadeTime, bool fadeOut, Image fadeImage)
    {
        float startAlpha, endAlpha;

        if (fadeOut)  // Fade image will fade from invisible to visible
        {
            startAlpha = 0.0f;
            endAlpha = 1.0f;
        }
        else          // Fade image will fade from visible to invisible
        {
            startAlpha = 1.0f;
            endAlpha = 0.0f;
        }
        yield return StartCoroutine(Fade(startAlpha, endAlpha, fadeTime, fadeImage));
    }

    /**
        @authors Darrick Hilburn, Linda Lane

        Fade fades an image in or out

        Core code written by Linda
        Modified by Darrick to accomodate a different engine
    */
    IEnumerator Fade(float startAlpha, float endAlpha, float fadeTime, Image fadeImage)
    {
        float elapsedTime = 0f;
        while (elapsedTime < fadeTime)
        {
            Color tempColor = fadeImage.color;
            tempColor.a = Mathf.Lerp(startAlpha, endAlpha, (elapsedTime / fadeTime));
            fadeImage.color = tempColor;
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        Color finalColor = fadeImage.color;
        finalColor.a = endAlpha;
        fadeImage.color = finalColor;
    }
    #endregion

    #endregion

    /**
        @author Darrick Hilburn

        KillEngine stops all coroutines and unlocks the cursor
    */
    public void KillEngine()
    {
        StopAllCoroutines();
        mouseLookScript.enabled = false;
        Cursor.lockState = CursorLockMode.None;
        //Time.timeScale = 0;
    }

    /**
        @author Darrick Hilburn

        ResetEngine resets the engine in the current scene
    */
    public void ResetEngine()
    {
        GameObject[] enemySpawns = GameObject.FindGameObjectsWithTag(WaypointEnumValues.ENEMY_SPAWN_TAG);
        foreach (GameObject spawn in enemySpawns)
            spawn.GetComponent<EnemySpawner>().EnemyKilled();
        Cursor.lockState = CursorLockMode.Confined;
        Time.timeScale = 1;
        player.transform.position = waypoints[0].transform.position;
        Cursor.lockState = CursorLockMode.Confined;
        StartCoroutine("MovementEngine");
        StartCoroutine("FacingsEngine");
        StartCoroutine("EffectsEngine");
    }

    /**
        @author Darrick Hilburn

        SetEffectsCanvas finds the splatter and fader images in the scene and assigns them on the engine
    */
    public void SetEffectsCanvas()
    {
        splatter = GameObject.FindGameObjectWithTag(WaypointEnumValues.SPLATTER_IMAGE_TAG).GetComponent<Image>();
        fader = GameObject.FindGameObjectWithTag(WaypointEnumValues.FADE_IMAGE_TAG).GetComponent<Image>();
    }

    /**
        @author Darrick Hilburn

        SetMouseScript gets the look script off the player object
    */
    public void SetMouseScript(GameObject lookingObject)
    {
        mouseLookScript = lookingObject.GetComponent<MouseLook>();
    }


    public void LoadNextLevel()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
    }
}
