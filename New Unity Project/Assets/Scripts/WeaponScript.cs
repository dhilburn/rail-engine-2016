﻿using UnityEngine;

[System.Serializable]
public class WeaponScript
{
    public string name;
    public GameObject projectile;
    public float bulletForce;
    [Range(1, 100)]
    public int damage;
    [Range(1, 100)]
    public int maxClipSize;
    public float cooldown;
    public float reloadTime;
    public DamageType damageType;
        
    int clipSize;

    public WeaponScript()
    {
        name = "";
        projectile = null;
        bulletForce = 1000;
        damage = 10;
        maxClipSize = 1;
        cooldown = 0;
        reloadTime = 0;
        damageType = DamageType.NONE;
    }

    public WeaponScript(string passName, GameObject passProjectile, float force, int passDamage, int maxClip, float passCooldown, float reload, DamageType passType)
    {
        name = passName;
        projectile = passProjectile;

        if (force < 1000)
            bulletForce = 1000;
        else if (force > 10000)
            bulletForce = 10000;
        else
            bulletForce = force;

        if (passDamage < 1)
            damage = 1;
        else if (passDamage > 100)
            damage = 100;
        else
            damage = passDamage;

        if (maxClip < 1)
            maxClipSize = 1;
        else if (maxClip > 100)
            maxClipSize = 100;
        else
            maxClipSize = maxClip;

        if (passCooldown < 0)
            cooldown = 0;
        else
            cooldown = passCooldown;

        if (reload < 0)
            reloadTime = 0;
        else
            reloadTime = reload;

        damageType = passType;
    }

    public static void WeaponAssign(WeaponScript data, WeaponScript assign)
    {
        assign.name = data.name;
        assign.projectile = data.projectile;
        assign.bulletForce = data.bulletForce;
        assign.damage = data.damage;
        assign.maxClipSize = data.maxClipSize;
        assign.cooldown = data.cooldown;
        assign.reloadTime = data.reloadTime;
        assign.damageType = data.damageType;
    }

    public void ResetClipSize(int newClipSize)
    {
        clipSize = newClipSize;
    }

    public int GetClipSize()
    {
        return clipSize;
    }

    public void FireBullet()
    {
        clipSize--;
    }

    
}
