﻿using UnityEngine;
using System.Collections;

public static class Logger
{
    public static bool logValues;

    public static void Log(string toLog, bool writeLog = false)
    {
        if(logValues)
        {
            Debug.Log(toLog);
            if(writeLog)
            {
                WriteString(toLog);
            }
        }
    }

    public static void LogA(string toLog, bool writeLog = false)
    {
        bool storedLog = logValues;
        logValues = true;
        Log(toLog, writeLog);
        logValues = storedLog;
    }

    static void WriteString(string toWrite)
    {
        //Write information to an external file
        //Find the file path
        // Create the file
        // Write info to the file
        //Close the stream
    }
}
